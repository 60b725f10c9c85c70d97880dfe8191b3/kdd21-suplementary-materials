def read_columns(filename):
    columns = []
    columns_bestfrac = []
    entry = []
    with open(filename) as file:
        for line in file:
            if not line.strip() and entry:
                if "bestfrac" in entry[0]:
                    entry[0] = entry[0].replace(".bestfrac", "")
                    columns_bestfrac.append(entry)
                else:
                    columns.append(entry)
                entry = []
            elif "Graph:" in line:
                graph_name = line.split()[-1].split("/")[-1]
                graph_name = graph_name.replace("out.", "").replace("_", "\_")
                entry = [graph_name]
            elif "Number of nodes" in line:
                entry.append(line.split()[-1])
            elif "Number of edges" in line:
                entry.append(line.split()[-1])
            elif "Number of future edges" in line:
                entry.append(line.split()[-1])
            elif "Number of components" in line:
                entry.append(line.split()[-1])
            elif "Size of largest component" in line:
                entry.append(line.split()[-1])

    return columns, columns_bestfrac

def print_table(columns, columns_bestfrac):
    for column, column_bestfrac in zip(columns, columns_bestfrac):
        assert(column[0] == column_bestfrac[0])
        assert(len(column) == len(column_bestfrac))

        #  final_column = [column[0], column[1], column[3], column[4], column_bestfrac[1], column_bestfrac[2], column_bestfrac[3]]
        final_column = [column[0], column[1], column[3], column_bestfrac[1], column_bestfrac[3]]

        for i in range(0,len(final_column)-1):
            print(final_column[i], end=" & ")
        print(final_column[-1] + " \\\\")

columns, columns_bestfrac = read_columns("../experiment_outputs/graph_info.txt")
print_table(columns, columns_bestfrac)
