#!/usr/bin/python3

import networkx as nx
import matplotlib.pyplot as plt
from timeit import default_timer as timer

def readGraph(filename):
    edges = []
    with open(filename, "r") as f:
        f.readline() # first meta data line
        f.readline() # second meta data line
        lines = f.read().splitlines()
        for line in lines:
            items = line.split(' ')
            edges.append((items[0], items[1]))

    G = nx.Graph()
    G.add_edges_from(edges)
    return G

def writePositions(filename, positions):
    with open(filename, "w") as f:
        for node_id, pos in positions.items():
            x = "{0:.15f}".format(pos[0])
            y = "{0:.15f}".format(pos[1])
            f.write(node_id + " " + x + " " + y + "\n")

def readPositions(filename):
    positions = {}
    with open(filename, "r") as f:
        lines = f.read().splitlines()
        for line in lines:
            items = line.split(' ')
            positions[items[0]] = (float(items[1]), float(items[2]))

    return positions

def layout(G):
    progs = [ 'neato', 'dot', 'twopi', 'fdp', 'sfdp', 'circo' ]
    positions = nx.drawing.nx_pydot.graphviz_layout(G, prog=progs[0])
    #  positions = nx.spring_layout(G)
    writePositions("positions.txt", positions)
    return positions

def plot(G, positions):
    plt.figure()
    plt.axis('off')
    node_color = ['red'] * len(G.nodes())
    nx.draw_networkx(G, pos=positions, node_shape='.', width=.02, node_size = .02, node_color = node_color, with_labels = False)
    plt.savefig('out.png', dpi=600)

graph_filename = "../../data/ca-cit-HepPh/out.ca-cit-HepPh"
#  graph_filename = "test_graph.txt"
G = readGraph(graph_filename)
# G = nx.fast_gnp_random_graph(500, .03)

positions_filename = "../dump/graph_layout_positions/positions_neato.txt"
#  positions = layout(G)
positions = readPositions(positions_filename)

Gc = max(nx.connected_component_subgraphs(G), key=len)
plot(Gc, positions)
