#!/bin/bash

graph_file="../../data/youtube-u-growth/out.youtube-u-growth"
k=5

echo ""
echo "Running naive algorithm:"
../build/./main "$graph_file" "$k" naive

echo ""
echo "Running postfix algorithm:"
../build/./main "$graph_file" "$k" postfix

echo ""
echo "Running starnode algorithm:"
../build/./main "$graph_file" "$k" starnode
