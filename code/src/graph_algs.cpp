#include "graph_algs.h"

GraphAlgs::GraphAlgs(IncrementalGraph const& graph)
	: graph(graph), is_destination(graph.numberOfNodes(), false)
	, distances(graph.numberOfNodes(), c::dist_max) {}

// This BFS sets the distance of start_id to 0, all others to infinity, and then
// starts a BFS from start_id.
void GraphAlgs::freshBFS(NodeID start_id, Distances& distances)
{
	BFS(start_id, distances, true);
}

// This BFS just updates nodes that are affected by a recent distance change of start_id.
void GraphAlgs::updateBFS(NodeID start_id, Distances& distances)
{
	BFS(start_id, distances, false);
}

Distance GraphAlgs::smallestDistance(NodeID source, NodeIDs const& destinations)
{
	setDestinations(destinations, true);

	if (is_destination[source]) {
		setDestinations(destinations, false);
		return 0;
	}

	std::queue<NodeID> Q;
	Q.push(source);
	distances.set(source, 0);

	while (!Q.empty()) {
		auto current_node_id = Q.front();
		Q.pop();

		for (auto neighbor_id: graph.getNeighborsOf(current_node_id)) {
			if (is_destination[neighbor_id]) {
				setDestinations(destinations, false);
				return distances[current_node_id] + 1;
			}

			if (distances[neighbor_id] > distances[current_node_id] + 1) {
				distances.set(neighbor_id, distances[current_node_id] + 1);
				Q.push(neighbor_id);
			}
		}
	}

	setDestinations(destinations, false);
	distances.reset();
	return c::dist_max;
}

Distance GraphAlgs::largestDistance(NodeID source, NodeID ignore)
{
	std::queue<NodeID> Q;
	Q.push(source);
	distances.set(source, 0);

	Distance max_distance = 0;
	while (!Q.empty()) {
		auto current_node_id = Q.front();
		Q.pop();
		max_distance = std::max(max_distance, distances[current_node_id]);

		for (auto neighbor_id: graph.getNeighborsOf(current_node_id)) {
			if (neighbor_id == ignore) { continue; }
			if (distances[neighbor_id] > distances[current_node_id] + 1) {
				distances.set(neighbor_id, distances[current_node_id] + 1);
				Q.push(neighbor_id);
			}
		}
	}

	distances.reset();
	return max_distance;
}

std::size_t GraphAlgs::numberOfComponents()
{
	auto uf = buildUnionFind();
	return uf.getNumberOfComponents();
}

std::size_t GraphAlgs::sizeOfLargestComponent()
{
	auto uf = buildUnionFind();
	return uf.sizeOfLargestComponent();
}

void GraphAlgs::BFS(NodeID start_id, Distances& distances, bool reset)
{
	if (reset) {
		std::fill(distances.begin(), distances.end(), c::dist_max);
		distances[start_id] = 0;
	}

	std::queue<NodeID> Q;
	Q.push(start_id);

	while (!Q.empty()) {
		auto current_node_id = Q.front();
		Q.pop();

		for (auto neighbor_id: graph.getNeighborsOf(current_node_id)) {
			if (distances[neighbor_id] > distances[current_node_id] + 1) {
				distances[neighbor_id] = distances[current_node_id] + 1;
				Q.push(neighbor_id);
			}
		}
	}
}

void GraphAlgs::setDestinations(NodeIDs const& destinations, bool value)
{
	for (auto const& destination: destinations) {
		is_destination[destination] = value;
	}
}

UnionFind<NodeID> GraphAlgs::buildUnionFind()
{
	UnionFind<NodeID> uf;
	uf.init(graph.numberOfNodes());

	for (auto node_id: graph.getNodeIDsRange()) {
		for (auto neighbor_id: graph.getNeighborsOf(node_id)) {
			uf.addRelation({node_id, neighbor_id});
		}
	}

	return uf;
}
