#pragma once

#include <cstdint>
#include <functional>
#include <limits>

// Typesafe ID class such that there are compiler errors if different IDs are
// mixed. The template parameter T is just there to assure this behavior.
// Additionally, we have a member function which can check for validity.
template <typename T>
struct ID
{
public:
	using IDType = uint32_t;
	static constexpr IDType invalid_value = std::numeric_limits<IDType>::max();

	ID(IDType id = invalid_value) : id(id) {}
	ID(ID const& other) = default;

	operator IDType() const { return id; }
	operator IDType&() { return id; }

	bool valid() const { return id != invalid_value; }
	void invalidate() { id = invalid_value; }

private:
	IDType id;
};

// define custom hash function to be able to use IDs with maps/sets
namespace std
{

template <typename T>
struct hash<ID<T>>
{
	using IDType = typename ID<T>::IDType;
	std::size_t operator()(ID<T> const& id) const noexcept
	{
		return std::hash<IDType>()(id);
	}
};

} // std
