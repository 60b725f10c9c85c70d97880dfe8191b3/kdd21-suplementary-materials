#include "incremental_k_center.h"

#include "basic_types.h"
#include "measurement_tool.h"

#include <algorithm>
#include <cmath>
#include <queue>
#include <numeric>
#include <limits>

// TODO: q to k if it is not provided explicitly
IncrementalKCenter::IncrementalKCenter(IncrementalGraph const& graph, Arguments const& args)
	: graph(graph), graph_algs(graph), k(args.k), q(args.q), epsilon(args.epsilon)
	, component_threshold(std::ceil(graph.numberOfNodes()/q)), update_algorithm(args.alg)
	, current_distance(c::dist_max), is_active(graph.numberOfNodes(), false)
	, number_of_active_components(0), far_nodes_distances_vec(k+1, Distances(graph.numberOfNodes()))
	, centers_distances_vec(k, Distances(graph.numberOfNodes()))
	, far_node_dist(graph.numberOfNodes(), c::dist_max), center_dist(graph.numberOfNodes())
	, closest_far_nodes(graph.numberOfNodes(), c::invalid_index), star_node_index(c::invalid_index)
	, in_boundary(graph.numberOfNodes(), false)
{
	assert(k > 0 && q > 0);
	// TODO: Assuming this just makes things easier for now. This can easily be
	// dropped at some point.
	assert((int)graph.numberOfNodes() >= k+1);

	init();
}

void IncrementalKCenter::init()
{
	initComponents();

	if (update_algorithm == UpdateAlgorithm::Randomized) {
		current_distance = 1.;
	}

	if (hasValidClustering()) {
		initialGonzalez(false);
	}
}

void IncrementalKCenter::initComponents()
{
	union_find.init(graph.numberOfNodes());

	for (auto node_id: graph.getNodeIDsRange()) {
		for (auto neighbor_id: graph.getNeighborsOf(node_id)) {
			union_find.addRelation({node_id, neighbor_id});
		}
	}

	for (auto node_id: graph.getNodeIDsRange()) {
		if (!is_active[node_id] && union_find.getComponentSize(node_id) >= component_threshold) {
			addComponentToActiveNodes(node_id);
			++number_of_active_components;
		}
	}
}

void IncrementalKCenter::initialGonzalez(bool count_updates)
{
	// run one round of general Gonzalez' algorithm to get a set of "tight" far nodes,
	// i.e., a set of far nodes, where there are no strictly further nodes.
	switch (update_algorithm) {
	case UpdateAlgorithm::TotallyNaive:
	case UpdateAlgorithm::Naive:
	case UpdateAlgorithm::Postfix:
	case UpdateAlgorithm::StarNode:
		runGonzalez(count_updates);
		break;
	case UpdateAlgorithm::NaiveSingleDist:
	case UpdateAlgorithm::StarNodeSingleDist:
		runGonzalezSingleDist(count_updates);
		break;
	case UpdateAlgorithm::Randomized:
		runGonzalezRandomized(count_updates);
		break;
	}
}

void IncrementalKCenter::updateGonzalez()
{
	if (use_non_naiv_gonzalez) {
		switch (update_algorithm) {
		case UpdateAlgorithm::TotallyNaive:
		case UpdateAlgorithm::Naive:
		case UpdateAlgorithm::Postfix:
		case UpdateAlgorithm::StarNode:
		case UpdateAlgorithm::NaiveSingleDist:
		case UpdateAlgorithm::Randomized:
			star_node_index = c::invalid_index;
			initialGonzalez();
			break;
		case UpdateAlgorithm::StarNodeSingleDist:
			updateGonzalezSingleDist();
			break;
		}
	}
	else {
		star_node_index = c::invalid_index;
		initialGonzalez();
	}
}

void IncrementalKCenter::updateGonzalezSingleDist()
{
	// for nodes in this order and while the closest distance is smaller than the furthest
	Distance last_max_distance = c::dist_max;
	std::size_t last_index = c::invalid_index;

	// we set this to avoid unwanted behavior in updateFarNodesSingleDist
	current_distance = c::dist_max;

	for (std::size_t i = 0; i < far_nodes.size(); ++i) {
		Distances min_far_node_dist(far_nodes.size(), c::dist_max);
		for (auto const& node_id: active_nodes) {
			// If this node is currently not in reach of any far node, ignore it.
			// This should only happen after two small components got merged into a large one.
			if (far_node_dist[node_id] == c::dist_max) { continue; }

			auto const far_node_index = closest_far_nodes[node_id];
			assert(far_node_index < far_nodes.size());

			// The far_node_dist of two neighboring nodes can differ at most by 1, thus
			// 2*far_node_dist[node_id] is a lower bound on the closest distance possible.
			if (2*far_node_dist[node_id] >= min_far_node_dist[far_node_index]) { continue; }

			for (auto const& neighbor_id: graph.getNeighborsOf(node_id)) {
				// We only check edges from smaller index to larger index nodes. Note that,
				// in particular, we only check between nodes with different associated far node!
				if (closest_far_nodes[neighbor_id] < far_node_index) {
					auto d = far_node_dist[node_id] + far_node_dist[neighbor_id] + 1;
					min_far_node_dist[far_node_index] = std::min(min_far_node_dist[far_node_index], d);
				}
			}
		}

		auto min_it = std::min_element(min_far_node_dist.begin(), min_far_node_dist.end());
		auto min_dist = *min_it;
		
		// The last node that was placed is also a closest node and thus the set without this
		// node is a clustering and thus we have a valid lower bound.
		if (min_dist >= last_max_distance) {
			break;
		}

		last_index = std::distance(min_far_node_dist.begin(), min_it);
		last_max_distance = updateFarNodesSingleDist(last_index);

		// if we did k iterations but we didn't break yet, run a clearn Gonzalez and return
		if (i == far_nodes.size()-1) {
			star_node_index = c::invalid_index;
			initialGonzalez();
			return;
		}
	}
	assert(last_index != c::invalid_index);

	current_distance = last_max_distance;
	// The centers are all far nodes except the max_node
	centers = far_nodes;
	std::swap(centers[last_index], centers.back());
	centers.pop_back();
	centers_changed_last_update = true;
}

void IncrementalKCenter::update(Edge const& edge)
{
	centers_changed_last_update = false;
	far_nodes_changed_last_update = false;
	distance_changed_last_update = false;
	auto const previous_far_node_updates = getFarNodeUpdateCount();
	auto const previous_distance = current_distance;

	// ignore loops
	if (edge.id1 == edge.id2) { return; }

	auto const had_valid_clustering = hasValidClustering();
	MEASUREMENT::start(EXP::UpdateComponents);
	updateComponents(edge.id1, edge.id2);
	MEASUREMENT::stop(EXP::UpdateComponents);

	// If none of the endpoints belongs to an active component, this edge insertion
	// does not influence the current clustering.
	if (!is_active[edge.id1] && !is_active[edge.id2]) { return; }

	// Note that after updating the components, we already know if a valid clustering
	// is possible or not, by knowing the number of large components.
	if (hasValidClustering()) {
		if (had_valid_clustering) {
			MEASUREMENT::start(EXP::IntroducedFarNode);
			bool const introduced_far_node = introducedFarNode(edge);
			MEASUREMENT::stop(EXP::IntroducedFarNode);

			if (!bothEndpointsFar(edge)) {
				MEASUREMENT::start(EXP::UpdateDistances);
				updateDistances(edge);
				MEASUREMENT::stop(EXP::UpdateDistances);
			}
			else {
				// This should only happen when two small components were merged into a large one.
				// In this case we have to run updateGonzalez, that's the reason of this assert.
				assert(introduced_far_node);
			}

			if (introduced_far_node) {
				MEASUREMENT::start(EXP::GonzalezAfterNodeGotFurther);
				updateGonzalez();
				inc_far_node_updates += getFarNodeUpdateCount() - previous_far_node_updates;
				MEASUREMENT::stop(EXP::GonzalezAfterNodeGotFurther);
			}
			else {
				MEASUREMENT::start(EXP::UpdateFarNodes);
				updateFarNodes();
				dec_far_node_updates += getFarNodeUpdateCount() - previous_far_node_updates;
				MEASUREMENT::stop(EXP::UpdateFarNodes);
			}
		}
		else {
			MEASUREMENT::start(EXP::GonzalezForNewValidClustering);
			initialGonzalez();
			inc_far_node_updates += getFarNodeUpdateCount() - previous_far_node_updates;
			MEASUREMENT::stop(EXP::GonzalezForNewValidClustering);
		}
	}

	far_nodes_changed_last_update = (previous_far_node_updates != getFarNodeUpdateCount());
	distance_changed_last_update = (previous_distance != current_distance);
}

bool IncrementalKCenter::introducedFarNode(Edge const& edge)
{
	Distance dist1 = c::dist_max, dist2 = c::dist_max;
	if (update_algorithm == UpdateAlgorithm::NaiveSingleDist || update_algorithm == UpdateAlgorithm::StarNodeSingleDist || update_algorithm == UpdateAlgorithm::Randomized) {
		dist1 = center_dist[edge.id1];
		dist2 = center_dist[edge.id2];
	}
	else {
		for (auto& distances: centers_distances_vec) {
			dist1 = std::min(dist1, distances[edge.id1]);
			dist2 = std::min(dist2, distances[edge.id2]);
		}
	}

	// This can only be the case if two small components were merged and then formed a large one.
	if (dist1 == c::dist_max && dist2 == c::dist_max) {
		assert(is_active[edge.id1] && is_active[edge.id2]);
		return true;
	}

	NodeID active_id, inactive_id;
	Distance center_distance;
	if (dist1 == c::dist_max) {
		active_id = edge.id2;
		inactive_id = edge.id1;
		center_distance = dist2;
	}
	else if (dist2 == c::dist_max) {
		active_id = edge.id1;
		inactive_id = edge.id2;
		center_distance = dist1;
	}
	else {
		return false;
	}

	assert(center_distance != c::dist_max);
	auto component_diameter = graph_algs.largestDistance(inactive_id, active_id);

	bool introduced_far_node = (center_distance + component_diameter + 1 > (1.+epsilon)*current_distance);
	if (introduced_far_node) { ++implicit_bucket_increase_count; }
	return introduced_far_node;
}

bool IncrementalKCenter::bothEndpointsFar(Edge const& edge)
{
	switch (update_algorithm) {
	case UpdateAlgorithm::Naive:
	case UpdateAlgorithm::Postfix:
	case UpdateAlgorithm::StarNode:
	case UpdateAlgorithm::TotallyNaive:
		for (auto& far_node_dist: far_nodes_distances_vec) {
			if (far_node_dist[edge.id1] != c::dist_max || far_node_dist[edge.id2] != c::dist_max) {
				return false;
			}
		}
		return true;
	case UpdateAlgorithm::NaiveSingleDist:
	case UpdateAlgorithm::StarNodeSingleDist:
	case UpdateAlgorithm::Randomized:
		return far_node_dist[edge.id1] == c::dist_max && far_node_dist[edge.id2] == c::dist_max;
	default:
		Error("Unknown update_algorithm!");
	};
}

bool IncrementalKCenter::hasValidClustering() const
{
	return number_of_active_components > 0 && (int)number_of_active_components <= k;
}

void IncrementalKCenter::updateComponents(NodeID node_id1, NodeID node_id2)
{
	if (union_find.belongToSameComponent(node_id1, node_id2)) {
		return;
	}

	auto size1 = union_find.getComponentSize(node_id1);
	auto size2 = union_find.getComponentSize(node_id2);

	union_find.addRelation({node_id1, node_id2});

	// two large components are merged
	if (size1 >= component_threshold && size2 >= component_threshold) {
		assert(is_active[node_id1] && is_active[node_id2]);
		--number_of_active_components;
	}
	// the small second component is merged into the large first
	else if (size1 >= component_threshold) {
		assert(is_active[node_id1] && !is_active[node_id2]);
		addComponentToActiveNodes(node_id2);
	}
	// the small first component is merged into the large second
	else if (size2 >= component_threshold) {
		assert(!is_active[node_id1] && is_active[node_id2]);
		addComponentToActiveNodes(node_id1);
	}
	// two small components are merged
	else if (size1 + size2 >= component_threshold) {
		assert(!is_active[node_id1] && !is_active[node_id2]);
		addComponentToActiveNodes(node_id1);
		++number_of_active_components;
	}
}

void IncrementalKCenter::addComponentToActiveNodes(NodeID start_id)
{
	NodeIDs stack;

	stack.push_back(start_id);
	active_nodes.push_back(start_id);
	is_active[start_id] = true;

	while (!stack.empty()) {
		auto current_id = stack.back();
		stack.pop_back();

		for (auto neighbor_id: graph.getNeighborsOf(current_id)) {
			if (!is_active[neighbor_id]) {
				stack.push_back(neighbor_id);
				active_nodes.push_back(neighbor_id);
				is_active[neighbor_id] = true;
			}
		}
	}
}

auto IncrementalKCenter::toUpdateAlgorithm(std::string const& update_algorithm_string)
	-> UpdateAlgorithm
{
	if (update_algorithm_string == "totallynaive") {
		return UpdateAlgorithm::TotallyNaive;
	}
	else if (update_algorithm_string == "naive") {
		return UpdateAlgorithm::Naive;
	}
	else if (update_algorithm_string == "naivesingledist") {
		return UpdateAlgorithm::NaiveSingleDist;
	}
	else if (update_algorithm_string == "postfix") {
		return UpdateAlgorithm::Postfix;
	}
	else if (update_algorithm_string == "starnode") {
		return UpdateAlgorithm::StarNode;
	}
	else if (update_algorithm_string == "starnodesingledist") {
		return UpdateAlgorithm::StarNodeSingleDist;
	}
	else if (update_algorithm_string == "randomized") {
		return UpdateAlgorithm::Randomized;
	}
	else {
		Error("Unknown update_algorithm_string: " << update_algorithm_string);
	}
}

std::string IncrementalKCenter::toString(UpdateAlgorithm update_algorithm)
{
	switch (update_algorithm) {
	case UpdateAlgorithm::TotallyNaive:
		return "totallynaive";
	case UpdateAlgorithm::Naive:
		return "naive";
	case UpdateAlgorithm::NaiveSingleDist:
		return "naivesingledist";
	case UpdateAlgorithm::Postfix:
		return "postfix";
	case UpdateAlgorithm::StarNode:
		return "starnode";
	case UpdateAlgorithm::StarNodeSingleDist:
		return "starnodesingledist";
	case UpdateAlgorithm::Randomized:
		return "randomized";
	default:
		Error("Unknown update_algorithm!");
	}
}

auto IncrementalKCenter::getAllUpdateAlgorithms() -> std::vector<UpdateAlgorithm>
{
	return {
		UpdateAlgorithm::TotallyNaive,
		UpdateAlgorithm::Naive,
		UpdateAlgorithm::NaiveSingleDist,
		UpdateAlgorithm::Postfix,
		UpdateAlgorithm::StarNode,
		UpdateAlgorithm::StarNodeSingleDist,
	};
}

std::size_t IncrementalKCenter::getFarNodeUpdateCount() const
{
	return far_node_updates;
}

std::size_t IncrementalKCenter::getDecFarNodeUpdateCount() const
{
	return dec_far_node_updates;
}

std::size_t IncrementalKCenter::getIncFarNodeUpdateCount() const
{
	return inc_far_node_updates;
}

std::size_t IncrementalKCenter::getImplicitBucketIncreaseCount() const
{
	return implicit_bucket_increase_count;
}

void IncrementalKCenter::useNonNaiveGonzalez(bool use_non_naiv_gonzalez)
{
	this->use_non_naiv_gonzalez = use_non_naiv_gonzalez;
}

void IncrementalKCenter::updateDistances(Edge const& edge)
{
	switch (update_algorithm) {
	case UpdateAlgorithm::TotallyNaive:
		updateDistancesTotallyNaive();
		break;
	case UpdateAlgorithm::Naive:
	case UpdateAlgorithm::Postfix:
	case UpdateAlgorithm::StarNode:
		updateFarNodesDistances(edge);
		break;
	case UpdateAlgorithm::NaiveSingleDist:
	case UpdateAlgorithm::StarNodeSingleDist:
	case UpdateAlgorithm::Randomized:
		updateClosestFarNodeDistances(edge);
		break;
	}
}

void IncrementalKCenter::updateDistancesTotallyNaive()
{
	// centers
	for (int i = 0; i < k; ++i) {
		graph_algs.freshBFS(centers[i], centers_distances_vec[i]);
	}

	// far nodes
	for (int i = 0; i < k+1; ++i) {
		graph_algs.freshBFS(far_nodes[i], far_nodes_distances_vec[i]);
	}
}

// This function can be called when a new edge was inserted and now we want to adapt
// the distances of all nodes from the far nodes. Note that if an endpoint of the edge
// changes its distance, then also its neighbors might have changed their distance.
// Thus, we check this recursively via BFS.
void IncrementalKCenter::updateFarNodesDistances(Edge const& edge)
{
	//
	// centers
	//

	for (int i = 0; i < k; ++i) {
		auto& distances = centers_distances_vec[i];
		// distances are int, no over-/underflow (even if any is c::dist_max)
		auto diff = distances[edge.id1] - distances[edge.id2];

		if (diff > 1) {
			assert(distances[edge.id2] != c::dist_max);
			// The first node has a larger distance, thus its distance changes by
			// the path from the center to the second node and over the new edge.
			distances[edge.id1] = distances[edge.id2] + 1;
			graph_algs.updateBFS(edge.id1, distances);
		}
		else if (diff < -1) {
			assert(distances[edge.id1] != c::dist_max);
			// Analogous to the previous case, except now the distance of the second
			// node is larger.
			distances[edge.id2] = distances[edge.id1] + 1;
			graph_algs.updateBFS(edge.id2, distances);
		}
	}

	//
	// far nodes
	//

	for (int i = 0; i < k+1; ++i) {
		auto& distances = far_nodes_distances_vec[i];
		// distances are int, no over-/underflow (even if any is c::dist_max)
		auto diff = distances[edge.id1] - distances[edge.id2];

		if (diff > 1) {
			assert(distances[edge.id2] != c::dist_max);
			// The first node has a larger distance, thus its distance changes by
			// the path from the center to the second node and over the new edge.
			distances[edge.id1] = distances[edge.id2] + 1;
			graph_algs.updateBFS(edge.id1, distances);
		}
		else if (diff < -1) {
			assert(distances[edge.id1] != c::dist_max);
			// Analogous to the previous case, except now the distance of the second
			// node is larger.
			distances[edge.id2] = distances[edge.id1] + 1;
			graph_algs.updateBFS(edge.id2, distances);
		}
	}
}

// Similar to updateFarNodesDistances, except that we just have to update a single vector.
// Also, already here we recognize if two far nodes got close and if so, we set a flag.
// Due to a detailed modification of the BFS, we do it here instead of in GraphAlgs.
void IncrementalKCenter::updateClosestFarNodeDistances(Edge const& edge)
{
	//
	// centers
	//

	// distances are int, no over-/underflow (even if any is c::dist_max)
	Distance diff = center_dist[edge.id1] - center_dist[edge.id2];

	if (diff > 1) {
		assert(center_dist[edge.id2] != c::dist_max);
		// The first node has a larger distance, thus its distance changes by
		// the path from the center to the second node and over the new edge.
		center_dist[edge.id1] = center_dist[edge.id2] + 1;
		graph_algs.updateBFS(edge.id1, center_dist);
	}
	else if (diff < -1) {
		assert(center_dist[edge.id1] != c::dist_max);
		// Analogous to the previous case, except now the distance of the second
		// node is larger.
		center_dist[edge.id2] = center_dist[edge.id1] + 1;
		graph_algs.updateBFS(edge.id2, center_dist);
	}

	//
	// far nodes
	//

	assert(far_node_dist[edge.id1] != c::dist_max || far_node_dist[edge.id2] != c::dist_max);

	star_node_index = c::invalid_index;
	// For consistency reasons we want to always choose the smaller index, if we
	// have the choice (i.e., if the star is a single edge). This alternative index
	// thus stores the other alternative, as long as we are not sure yet if we
	// are in the case where we cannot choose.
	std::size_t alternative_index = c::invalid_index;

	// distances are int, no over-/underflow (even if one of them is c::dist_max)
	diff = far_node_dist[edge.id1] - far_node_dist[edge.id2];
	// If one of the endpoints was in a previously inactive component, far nodes couldn't
	// get close, because it cannot lie on a shortest path between far nodes.
	bool far_nodes_got_close = false;
	if (far_node_dist[edge.id1] != c::dist_max && far_node_dist[edge.id2] != c::dist_max) {
		far_nodes_got_close = (closest_far_nodes[edge.id1] != closest_far_nodes[edge.id2] &&
			far_node_dist[edge.id1] + far_node_dist[edge.id2] + 1 < current_distance);
	}

	if (std::abs(diff) <= 1) {
		// No distance can changes in this case! But maybe two far nodes got close.
		if (far_nodes_got_close) {
			// This is the only node pair that got close, so just pick id1 arbitrarily.
			star_node_index = std::min(closest_far_nodes[edge.id1], closest_far_nodes[edge.id2]);
		}
		return;
	}

	NodeID start_id;
	if (diff > 1) {
		// The first node has a larger distance, thus its distance changes by
		// the path from the center to the second node and over the new edge.
		if (far_nodes_got_close) {
			star_node_index = closest_far_nodes[edge.id2];
			alternative_index = closest_far_nodes[edge.id1];
		}

		far_node_dist[edge.id1] = far_node_dist[edge.id2] + 1;
		closest_far_nodes[edge.id1] = closest_far_nodes[edge.id2];
		start_id = edge.id1;
	}
	else { // diff < -1
		// Analogous to the previous case, except now the distance of the second
		// node is larger.
		if (far_nodes_got_close) {
			star_node_index = closest_far_nodes[edge.id1];
			alternative_index = closest_far_nodes[edge.id2];
		}

		far_node_dist[edge.id2] = far_node_dist[edge.id1] + 1;
		closest_far_nodes[edge.id2] = closest_far_nodes[edge.id1];
		start_id = edge.id2;
	}

	std::queue<NodeID> Q;
	Q.push(start_id);

	while (!Q.empty()) {
		auto current_node_id = Q.front();
		Q.pop();

		for (auto neighbor_id: graph.getNeighborsOf(current_node_id)) {
			// first check if two far nodes got close
			if (star_node_index == c::invalid_index || alternative_index != c::invalid_index) { // only if necessary
				if (far_node_dist[neighbor_id] != c::dist_max &&
					closest_far_nodes[current_node_id] != closest_far_nodes[neighbor_id] &&
					far_node_dist[current_node_id] + far_node_dist[neighbor_id] + 1 < current_distance) {
					if (star_node_index == c::invalid_index) {
						star_node_index = closest_far_nodes[current_node_id];
						alternative_index = closest_far_nodes[neighbor_id];
					}
					else {
						if (alternative_index != closest_far_nodes[neighbor_id]) {
							alternative_index = c::invalid_index;
						}
					}
				}
			}

			// now do the normal BFS exploration
			if (far_node_dist[neighbor_id] > far_node_dist[current_node_id] + 1) {
				far_node_dist[neighbor_id] = far_node_dist[current_node_id] + 1;
				closest_far_nodes[neighbor_id] = closest_far_nodes[current_node_id];
				Q.push(neighbor_id);
			}
		}
	}

	// If the star graph is a single edge, take the far node with smaller index.
	if (alternative_index != c::invalid_index) {
		star_node_index = std::min(star_node_index, alternative_index);
	}
}

void IncrementalKCenter::updateFarNodes()
{
	switch (update_algorithm) {
	case UpdateAlgorithm::TotallyNaive:
	case UpdateAlgorithm::Naive:
		updateFarNodesNaive();
		break;
	case UpdateAlgorithm::NaiveSingleDist:
		updateFarNodesNaiveSingleDist();
		break;
	case UpdateAlgorithm::Postfix:
		updateFarNodesPostfix();
		break;
	case UpdateAlgorithm::StarNode:
		updateFarNodesStarNode();
		break;
	case UpdateAlgorithm::StarNodeSingleDist:
		updateFarNodesStarNodeSingleDist();
		break;
	case UpdateAlgorithm::Randomized:
		updateFarNodesRandomized();
		break;
	}
}

void IncrementalKCenter::updateFarNodesNaive()
{
	// check if there are violating indices
	for (int i = 0; i < k; ++i) {
		auto const& distances = far_nodes_distances_vec[i];
		for (int j = i+1; j < k+1; ++j) {
			auto other_far_node = far_nodes[j];
			if (distances[other_far_node] < current_distance) {
				// if the far nodes are invalid, compute them from scratch and we are done
				runGonzalez();
				return;
			}
		}
	}
}

void IncrementalKCenter::updateFarNodesNaiveSingleDist()
{
	// a set star node index indicates that at least two far nodes got close
	if (star_node_index != c::invalid_index) {
		runGonzalezSingleDist();
	}
}

void IncrementalKCenter::updateFarNodesRandomized()
{
	if (star_node_index == c::invalid_index) { return; }
	if (star_node_index == 0) { runGonzalezRandomized(); return; }

	assert(star_node_index != c::invalid_index);

	// delete all far_nodes starting from star_node_index
	far_nodes.resize(star_node_index);

	number_of_far_nodes = 0;

	//
	// find boundary
	//

	boundary.clear();
	in_boundary.reset();
	for (NodeID node_id: active_nodes) {
		// ignore nodes close to the surviving far_nodes
		if (closest_far_nodes[node_id] < star_node_index) {
			if (far_node_dist[node_id] >= current_distance) {
				++number_of_far_nodes;
			}
			continue;
		}

		++number_of_far_nodes;
		closest_far_nodes[node_id] = c::invalid_index;
		far_node_dist[node_id] = c::dist_max;

		for (auto neighbor_id: graph.getNeighborsOf(node_id)) {
			if (!in_boundary[neighbor_id] && closest_far_nodes[neighbor_id] < star_node_index) {
				in_boundary.set(neighbor_id, true);
				boundary.push_back(neighbor_id);
			}
		}
	}
	assert(!boundary.empty());

	//
	// expand boundary
	//

	// sort boundary in decreasing distance
	auto dec_far_node_dist = [&](NodeID node_id1, NodeID node_id2) {
		return far_node_dist[node_id1] > far_node_dist[node_id2];
	};
	std::sort(boundary.begin(), boundary.end(), dec_far_node_dist);

	std::queue<NodeID> todo;
	todo.push(boundary.back());

	// update from boundary to inside
	while (!todo.empty()) {
		auto node_id = todo.front();
		todo.pop();

		// before expanding, insert all the boundary nodes of this distance
		while (!boundary.empty() && far_node_dist[boundary.back()] == far_node_dist[node_id]) {
			todo.push(boundary.back());
			boundary.pop_back();
		}
		
		// don't expland nodes at maximal distance
		if (far_node_dist[node_id] >= current_distance) { continue; }

		// update the neighbors
		for (auto neighbor_id: graph.getNeighborsOf(node_id)) {
			if (closest_far_nodes[neighbor_id] == c::invalid_index) {
				closest_far_nodes[neighbor_id] = closest_far_nodes[node_id];
				far_node_dist[neighbor_id] = far_node_dist[node_id] + 1;
				if (far_node_dist[neighbor_id] < current_distance) { --number_of_far_nodes; }
				todo.push(neighbor_id);
			}
		}
	}

	//
	// run "incremental randomized Gonzalez"
	//

	runGonzalezRandomized();
}

void IncrementalKCenter::updateFarNodesPostfix()
{
	// get the minimum over all the maxima from the two indices of the violating pairs
	std::size_t min_max_index = k+2;
	for (int i = 0; i < k; ++i) {
		auto const& distances = far_nodes_distances_vec[i];
		for (int j = i+1; j < k+1; ++j) {
			auto other_far_node = far_nodes[j];
			if (distances[other_far_node] < current_distance) {
				min_max_index = std::min<std::size_t>(min_max_index, j);
			}
		}
	}

	if ((int)min_max_index != k+2) {
		// If the far nodes are invalid, use Gonzalez algorithm to fix all the far nodes
		// with larger indices than the min max index.
		runGonzalezFrom(min_max_index);
	}
}

// TODO: explain star node lemma briefly
void IncrementalKCenter::updateFarNodesStarNode()
{
	NodeIDs violating_indices;
	for (int i = 0; i < k; ++i) {
		auto const& distances = far_nodes_distances_vec[i];
		for (int j = i+1; j < k+1; ++j) {
			auto other_far_node = far_nodes[j];
			if (distances[other_far_node] < current_distance) {
				violating_indices.push_back(i);
				violating_indices.push_back(j);
			}
		}
	}

	// if the far nodes are still valid, then there's nothing to do
	if (violating_indices.empty()) { return; }

	if (violating_indices.size() == 2) {
		star_node_index = violating_indices.front(); // just take any
	}
	else {
		std::sort(violating_indices.begin(), violating_indices.end());
		auto star_node_it = std::adjacent_find(violating_indices.begin(), violating_indices.end());
		star_node_index = *star_node_it;

		assert(*star_node_it == *std::adjacent_find(violating_indices.rbegin(), violating_indices.rend()));
	}

	// We now remove the star node from the far nodes, thus we have to recompute
	// the closest far node for each node. This gives us the max_distance and
	// the max_node.
	Distances min_distances(graph.numberOfNodes(), c::dist_max);
	for (std::size_t far_node_index = 0; far_node_index < far_nodes.size(); ++far_node_index) {
		// skip if this is the far node we are removing...
		if (far_node_index != star_node_index) {
			updateMinDistances(far_nodes_distances_vec[far_node_index], min_distances);
		}
	}

	NodeID max_node_id = 0;
	Distance max_distance = -1;
	for (auto node_id: active_nodes) {
		if (min_distances[node_id] > max_distance) {
			max_distance = min_distances[node_id];
			max_node_id = node_id;
		}
	}

	// Make max_node a far node
	far_nodes[star_node_index] = max_node_id;
	++far_node_updates;
	graph_algs.freshBFS(max_node_id, far_nodes_distances_vec[star_node_index]);

	// Note that max_distance can actually be larger than current_distance!
	// However, we then already saw a certificate for a smaller center distance,
	// so we keep the smaller distance.
	if (max_distance < current_distance) {
		current_distance = max_distance;
		// The centers are all far nodes except the max_node
		centers = far_nodes;
		std::swap(centers[star_node_index], centers.back());
		centers.pop_back();

		// TODO: this might be costly; make it faster and check timing
		centers_distances_vec = far_nodes_distances_vec;
		std::swap(centers_distances_vec[star_node_index], centers_distances_vec.back());
		centers_distances_vec.pop_back();

		centers_changed_last_update = true;
	}
}

void IncrementalKCenter::updateFarNodesStarNodeSingleDist()
{
	if (star_node_index == c::invalid_index) { return; }

	auto max_distance = updateFarNodesSingleDist(star_node_index);

	// Note that max_distance can actually be larger than current_distance!
	// However, we then already saw a certificate for a smaller center distance,
	// so we keep the smaller distance.
	if (max_distance < current_distance) {
		current_distance = max_distance;
		// The centers are all far nodes except the max_node
		centers = far_nodes;
		std::swap(centers[star_node_index], centers.back());
		centers.pop_back();
		centers_changed_last_update = true;
	}
}

Distance IncrementalKCenter::updateFarNodesSingleDist(std::size_t far_node_index)
{
	MEASUREMENT::start(EXP::DummyTimer1);
	boundary.clear();
	in_boundary.reset();
	// for each node, if it is in the star_node cluster, check if it is a boundary node
	for (auto node_id: active_nodes) {
		if (closest_far_nodes[node_id] == far_node_index) {
			for (auto neighbor_id: graph.getNeighborsOf(node_id)) {
				if (in_boundary[neighbor_id]) {
					continue;
				}
				if (closest_far_nodes[neighbor_id] != far_node_index) {
					in_boundary.set(neighbor_id, true);
					boundary.push_back(neighbor_id);
				}
			}
		}
	}
	assert(!boundary.empty());

	MEASUREMENT::stop(EXP::DummyTimer1);

	MEASUREMENT::start(EXP::DummyTimer2);
	// sort boundary in decreasing distance
	auto dec_far_node_dist = [&](NodeID node_id1, NodeID node_id2) {
		return far_node_dist[node_id1] > far_node_dist[node_id2];
	};
	std::sort(boundary.begin(), boundary.end(), dec_far_node_dist);
	MEASUREMENT::stop(EXP::DummyTimer2);

	MEASUREMENT::start(EXP::DummyTimer3);

	std::queue<NodeID> todo;
	todo.push(boundary.back());

	// update from boundary to inside
	while (!todo.empty()) {
		auto node_id = todo.front();
		todo.pop();

		// before expanding, insert all the boundary nodes of this distance
		while (!boundary.empty() && far_node_dist[boundary.back()] == far_node_dist[node_id]) {
			todo.push(boundary.back());
			boundary.pop_back();
		}

		// update the neighbors
		for (auto neighbor_id: graph.getNeighborsOf(node_id)) {
			if (closest_far_nodes[neighbor_id] == far_node_index) {
				far_node_dist[neighbor_id] = far_node_dist[node_id] + 1;
				closest_far_nodes[neighbor_id] = closest_far_nodes[node_id];
				todo.push(neighbor_id);
			}
		}
	}
	MEASUREMENT::stop(EXP::DummyTimer3);

	MEASUREMENT::start(EXP::DummyTimer4);
	NodeID max_node_id = 0;
	Distance max_distance = -1;
	for (auto node_id: active_nodes) {
		if (far_node_dist[node_id] > max_distance) {
			max_distance = far_node_dist[node_id];
			max_node_id = node_id;
		}
	}
	MEASUREMENT::stop(EXP::DummyTimer4);

	MEASUREMENT::start(EXP::DummyTimer5);
	// Make max_node a far node
	far_nodes[far_node_index] = max_node_id;
	++far_node_updates;
	if (max_distance < current_distance) {
		// assign distances to center_dist before we update the distances from the star node
		center_dist = far_node_dist;
	}

	auto set_close = [&](NodeID node_id) {
		closest_far_nodes[node_id] = far_node_index;
	};
	graph_algs.updateBFS(max_node_id, far_node_dist, set_close);
	MEASUREMENT::stop(EXP::DummyTimer5);

	return max_distance;
}

void IncrementalKCenter::runGonzalez(bool count_updates)
{
	far_nodes.clear();
	current_distance = c::dist_max;

	Distances min_distances(graph.numberOfNodes(), c::dist_max);

	// choose initial far node and make BFS
	// TODO: maybe randomize the first node?
	far_nodes.push_back(active_nodes.front());
	if (count_updates) { ++far_node_updates; }
	graph_algs.freshBFS(far_nodes.back(), far_nodes_distances_vec[far_nodes.size()-1]);
	updateMinDistances(far_nodes_distances_vec[far_nodes.size()-1], min_distances);

	while ((int)far_nodes.size() != k+1) {
		// choose the furthest node remaining
		NodeID max_node_id = 0;
		Distance max_distance = -1;
		for (auto node_id: active_nodes) {
			if (min_distances[node_id] > max_distance) {
				max_distance = min_distances[node_id];
				max_node_id = node_id;
			}
		}
		current_distance = std::min(current_distance, max_distance);

		// Make max_node a far node
		far_nodes.push_back(max_node_id);
		if (count_updates) { ++far_node_updates; }
		graph_algs.freshBFS(max_node_id, far_nodes_distances_vec[far_nodes.size()-1]);
		updateMinDistances(far_nodes_distances_vec[far_nodes.size()-1], min_distances);
	}

	// The last node that was added is in distance "current_distance" to at least
	// one of the far nodes. Thus, without it we have a k-center clustering of
	// distance "current_distance".
	centers = far_nodes;
	centers.pop_back();

	// TODO: this might be costly; make it faster and check timing
	centers_distances_vec = far_nodes_distances_vec;
	centers_distances_vec.pop_back();

	centers_changed_last_update = true;
}

void IncrementalKCenter::runGonzalezSingleDist(bool count_updates)
{
	auto set_close = [&](NodeID node_id) {
		closest_far_nodes[node_id] = far_nodes.size()-1;
	};

	far_nodes.clear();
	std::fill(far_node_dist.begin(), far_node_dist.end(), c::dist_max);
	std::fill(closest_far_nodes.begin(), closest_far_nodes.end(), c::invalid_index);
	current_distance = c::dist_max;

	// choose initial far node and make BFS
	// TODO: maybe randomize the first node?
	far_nodes.push_back(active_nodes.front());
	if (count_updates) { ++far_node_updates; }
	graph_algs.updateBFS(far_nodes.back(), far_node_dist, set_close);

	while ((int)far_nodes.size() != k+1) {
		// choose the furthest node remaining
		NodeID max_node_id = 0;
		Distance max_distance = -1;
		for (auto node_id: active_nodes) {
			if (far_node_dist[node_id] > max_distance) {
				max_distance = far_node_dist[node_id];
				max_node_id = node_id;
			}
		}
		current_distance = std::min(current_distance, max_distance);

		// Make max_node a far node
		far_nodes.push_back(max_node_id);
		if (count_updates) { ++far_node_updates; }
		graph_algs.updateBFS(far_nodes.back(), far_node_dist, set_close);

		if ((int)far_nodes.size() == k) {
			center_dist = far_node_dist;
		}
	}

	// The last node that was added is in distance "current_distance" to at least
	// one of the far nodes. Thus, without it we have a k-center clustering of
	// distance "current_distance".
	centers = far_nodes;
	centers.pop_back();
	centers_changed_last_update = true;
}

void IncrementalKCenter::runGonzalezFrom(std::size_t first_index)
{
	assert(first_index > 0 && (int)first_index <= k+1);

	far_nodes.resize(first_index-1);
	Distances min_distances(graph.numberOfNodes(), c::dist_max);
	for (std::size_t far_node_index = 0; far_node_index < first_index; ++far_node_index) {
		updateMinDistances(far_nodes_distances_vec[far_node_index], min_distances);
	}

	while ((int)far_nodes.size() != k+1) {
		// choose the furthest node remaining
		NodeID max_node_id = 0;
		Distance max_distance = -1;
		for (auto node_id: active_nodes) {
			if (min_distances[node_id] > max_distance) {
				max_distance = min_distances[node_id];
				max_node_id = node_id;
			}
		}
		current_distance = std::min(current_distance, max_distance);

		// Make max_node a far node
		far_nodes.push_back(max_node_id);
		++far_node_updates;
		graph_algs.freshBFS(max_node_id, far_nodes_distances_vec[far_nodes.size()-1]);
		updateMinDistances(far_nodes_distances_vec[far_nodes.size()-1], min_distances);
	}

	// The last node that was added is in distance "current_distance" to at least
	// one of the far nodes. Thus, without it we have a k-center clustering of
	// distance "current_distance".
	centers = far_nodes;
	centers.pop_back();

	// TODO: this might be costly; make it faster and check timing
	centers_distances_vec = far_nodes_distances_vec;
	centers_distances_vec.pop_back();

	centers_changed_last_update = true;
}

void IncrementalKCenter::runGonzalezRandomized(bool count_updates)
{
	assert(current_distance != c::dist_max);

	NodeIDs far_nodes;
	NodeIDs centers;
	Distances far_node_dist;
	Distances center_dist;
	std::vector<std::size_t> closest_far_nodes;

	// TODO: These function copy a lot, but this was the clearest way to handle things.
	// To improve the running time, we should try to avoid copies here!
	auto store_far_nodes = [&]() {
		far_nodes = this->far_nodes;
		far_node_dist = this->far_node_dist;
		closest_far_nodes = this->closest_far_nodes;
	};
	auto restore_far_nodes = [&]() {
		this->far_nodes = far_nodes;
		this->far_node_dist = far_node_dist;
		this->closest_far_nodes = closest_far_nodes;
	};
	auto store_centers = [&]() {
		centers = this->centers;
		center_dist = this->center_dist;
	};
	auto restore_centers = [&]() {
		this->centers = centers;
		this->center_dist = center_dist;
	};
	auto max_active_center_dist = [&]() {
		auto max_distance = -1;
		for (auto node_id: graph.getNodeIDsRange()) {
			if (is_active[node_id] && this->center_dist[node_id] > max_distance) {
				max_distance = this->center_dist[node_id];
			}
		}
		return max_distance;
	};

	while (true) {
		runGonzalezRandomizedInnerLoop(count_updates);
		star_node_index = c::invalid_index;
		if ((int)this->far_nodes.size() != k+1) {
			if (!far_nodes.empty()) {
				restore_far_nodes();
				return;
			}
			current_distance = current_distance/(1.+epsilon);
			// TODO: are there issues with removing this? If so, why?
			// // TODO: For very small graphs we might return here without having k+1 far nodes.
			// if (current_distance == 0) {
			//     current_distance = 1; return;
			// }
			store_centers();
		}
		else if (max_active_center_dist() > (1.+epsilon)*current_distance) {
			current_distance = current_distance*(1.+epsilon);
			if (!centers.empty()) {
				restore_centers();
				return;
			}
			store_far_nodes();
		}
		else {
			// far nodes and centers are already stored
			return;
		}
	}
}

void IncrementalKCenter::runGonzalezRandomizedInnerLoop(bool count_updates)
{
	auto set_close = [&](NodeID node_id) {
		closest_far_nodes[node_id] = far_nodes.size()-1;
	};

	// Either if the star node is the first node and we thus reset all far nodes,
	// or this is a fresh run.
	if (star_node_index == 0 || star_node_index == c::invalid_index) {
		far_nodes.clear();
		std::fill(far_node_dist.begin(), far_node_dist.end(), c::dist_max);
		std::fill(closest_far_nodes.begin(), closest_far_nodes.end(), c::invalid_index);
		number_of_far_nodes = active_nodes.size();

		// choose initial far node and make BFS
		// TODO: maybe randomize the first node?
		far_nodes.push_back(active_nodes.front());
		if (count_updates) { ++far_node_updates; } // TODO: The counting is off... but we ignore it anyway
		graph_algs.updateBFSWithFarNodeCount(far_nodes.back(), far_node_dist, set_close, current_distance, number_of_far_nodes);
	}

	while (number_of_far_nodes > 0 && (int)far_nodes.size() != k+1) {
		// choose a node which is far from all centers uniformly at random
		std::size_t random_count = random.getSizeT(1, number_of_far_nodes);
		std::size_t counter = 0;
		NodeID new_center_id;
		for (NodeID node_id: active_nodes) {
			if (far_node_dist[node_id] >= current_distance) {
				++counter;
				if (counter == random_count) {
					new_center_id = node_id;
					break;
				}
			}
		}

		assert(new_center_id.valid());

		// Make max_node a far node
		far_nodes.push_back(new_center_id);
		if (count_updates) { ++far_node_updates; } // TODO: The counting is off... but we ignore it anyway
		graph_algs.updateBFSWithFarNodeCount(far_nodes.back(), far_node_dist, set_close, current_distance, number_of_far_nodes);

		if ((int)far_nodes.size() == k) {
			center_dist = far_node_dist;
		}
	}

	// The last node that was added is in distance "current_distance" to at least
	// one of the far nodes. Thus, without it we have a k-center clustering of
	// distance "current_distance".
	centers = far_nodes;
	centers.pop_back();
	centers_changed_last_update = true;
}

bool IncrementalKCenter::centersChangedLastUpdate() const
{
	return centers_changed_last_update;
}

bool IncrementalKCenter::farNodesChangedLastUpdate() const
{
	return far_nodes_changed_last_update;
}

bool IncrementalKCenter::distanceChangedLastUpdate() const
{
	return distance_changed_last_update;
}

NodeIDs const& IncrementalKCenter::getCurrentCenters() const
{
	assert(hasValidClustering());
	return centers;
}

NodeIDs const& IncrementalKCenter::getCurrentFarNodes() const
{
	assert(hasValidClustering());
	return far_nodes;
}

Distance IncrementalKCenter::currentKCenterDistance() const
{
	assert(hasValidClustering());
	return current_distance;
}

NodeIDs const& IncrementalKCenter::getActiveNodes() const
{
	return active_nodes;
}

bool IncrementalKCenter::isActive(NodeID node_id) const
{
	return is_active[node_id];
}

std::size_t IncrementalKCenter::getNumberOfActiveComponents() const
{
	return number_of_active_components;
}

Clustering IncrementalKCenter::computeClusteringVector()
{
	assert(hasValidClustering());

	Clustering clustering(graph.numberOfNodes());

	switch (update_algorithm) {
	case UpdateAlgorithm::Naive:
	case UpdateAlgorithm::Postfix:
	case UpdateAlgorithm::StarNode:
	case UpdateAlgorithm::TotallyNaive:
		for (auto node_id: active_nodes) {
			std::size_t min_index;
			Distance min_distance = c::dist_max;
			for (std::size_t i = 0; i < far_nodes_distances_vec.size(); ++i) {
				auto dist = far_nodes_distances_vec[i][node_id];
				if (dist < min_distance) {
					min_index = i;
					min_distance = dist;
				}
			}
			clustering[node_id] = far_nodes[min_index];
		}
		break;
	case UpdateAlgorithm::NaiveSingleDist:
	case UpdateAlgorithm::StarNodeSingleDist:
	case UpdateAlgorithm::Randomized:
		for (auto node_id: active_nodes) {
			clustering[node_id] = far_nodes[closest_far_nodes[node_id]];
		}
		break;
	};

	return clustering;
}

void IncrementalKCenter::updateMinDistances(Distances& distances, Distances& min_distances)
{
	for (auto node_id: active_nodes) {
		min_distances[node_id] = std::min(min_distances[node_id], distances[node_id]);
	}
}
