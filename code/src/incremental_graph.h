#pragma once

#include "basic_types.h"
#include "defs.h"
#include "range.h"

#include <string>

namespace unit_tests { void testIncrementalGraph(); }

class IncrementalGraph
{
public:
	IncrementalGraph(std::string const& filename, bool connect_and_reduce = false);

	std::size_t numberOfNodes() const;
	std::size_t numberOfEdges() const;
	std::size_t numberOfFutureEdges() const;

	bool hasNextEdge() const;
	Edge addNextEdge();
	// returns true iff the edge was added (i.e., if it didn't exist yet)
	bool addUnplannedEdge(Edge const& new_edge);

	ValueRange<NodeID> getNodeIDsRange() const;
	NodeIDs const& getNeighborsOf(NodeID node_id) const;

	NodeID toInputID(NodeID node_id) const;

private:
	std::size_t number_of_edges;

	// the future_edges are sorted such that the next edge is at the back
	Edges future_edges;
	// adjacency list
	std::vector<NodeIDs> neighbors_vec;

	NodeIDs to_input_id;

	void readGraph(std::string const& filename, bool connect_and_reduce);
	bool isValidEdge(Edge const& edge) const;
	void addEdge(Edge const& edge);

	// functions for preprocessing
	void greedilyConnectAndReduceGraph(NodeIDs& node_ids, Edges& edges_to_add);
};
