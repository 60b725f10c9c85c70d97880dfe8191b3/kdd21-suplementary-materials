// Make sure to always print in the unit tests
#ifdef NVERBOSE
#	undef NVERBOSE
#endif
#include "unit_tests.h"

#include "defs.h"
#include "graph_algs.h"
#include "incremental_graph.h"
#include "incremental_k_center.h"
#include "incremental_k_center_randomized.h"

//
// Define some helpers
//

#define TEST(x)                                                                \
	do {                                                                       \
		if (!(x)) {                                                            \
			std::cout << "\n";                                                 \
			std::cout << "TEST_FAILED!\n";                                     \
			std::cout << "File: " << __FILE__ << "\n";                         \
			std::cout << "Line: " << __LINE__ << "\n";                         \
			std::cout << "Function: " << __func__ << "\n";                     \
			std::cout << "Test: " << #x << "\n";                               \
			std::cout << "\n";                                                 \
			std::cout << std::flush;                                           \
			std::abort();                                                      \
		}                                                                      \
	} while (0)

// data sets
namespace
{
	std::vector<std::string> const FILENAMES = {
		"../../data/ca-cit-HepPh/out.ca-cit-HepPh",
		"../../data/ca-cit-HepTh/out.ca-cit-HepTh",
		"../../data/chess/out.chess",
		// "../../data/contact/out.contact", // small
		"../../data/dblp_coauthor/out.dblp_coauthor",
		"../../data/digg-friends/out.digg-friends",
		"../../data/dnc-temporalGraph/out.dnc-temporalGraph",
		"../../data/elec/out.elec",
		"../../data/enron/out.enron",
		"../../data/epinions/out.epinions",
		"../../data/facebook-wosn-links/out.facebook-wosn-links",
		"../../data/facebook-wosn-wall/out.facebook-wosn-wall",
		"../../data/flickr-growth/out.flickr-growth", // 12
		"../../data/link-dynamic-simplewiki/out.link-dynamic-simplewiki",
		"../../data/lkml-reply/out.lkml-reply",
		// "../../data/mit/out.mit", // small
		// "../../data/moreno_vdb/out.moreno_vdb_vdb", // small
		"../../data/munmun_digg_reply/out.munmun_digg_reply",
		"../../data/opsahl-ucsocial/out.opsahl-ucsocial",
		"../../data/prosper-loans/out.prosper-loans",
		// "../../data/radoslaw_email/out.radoslaw_email_email", // small
		"../../data/slashdot-threads/out.slashdot-threads",
		// // "../../data/sociopatterns-hypertext/out.sociopatterns-hypertext", // small
		// "../../data/sociopatterns-infectious/out.sociopatterns-infectious", // small
		"../../data/topology/out.topology",
		"../../data/wikipedia-growth/out.wikipedia-growth", // 25
		"../../data/wiki_talk_ar/out.wiki_talk_ar",
		"../../data/wiki_talk_de/out.wiki_talk_de",
		"../../data/wiki_talk_en/out.wiki_talk_en", // 28
		"../../data/wiki_talk_es/out.wiki_talk_es",
		"../../data/wiki_talk_fr/out.wiki_talk_fr",
		"../../data/wiki_talk_it/out.wiki_talk_it",
		"../../data/wiki_talk_nl/out.wiki_talk_nl",
		"../../data/wiki_talk_pt/out.wiki_talk_pt",
		"../../data/wiki_talk_ru/out.wiki_talk_ru",
		"../../data/wiki_talk_zh/out.wiki_talk_zh"
	};

	std::vector<std::string> const FILENAMES_BESTFRAC = {
		"../../data/ca-cit-HepPh/out.ca-cit-HepPh.bestfrac",
		"../../data/ca-cit-HepTh/out.ca-cit-HepTh.bestfrac",
		"../../data/chess/out.chess.bestfrac",
		// "../../data/contact/out.contact.bestfrac",
		"../../data/dblp_coauthor/out.dblp_coauthor.bestfrac",
		"../../data/digg-friends/out.digg-friends.bestfrac",
		"../../data/dnc-temporalGraph/out.dnc-temporalGraph.bestfrac",
		"../../data/elec/out.elec.bestfrac",
		"../../data/enron/out.enron.bestfrac",
		"../../data/epinions/out.epinions.bestfrac",
		"../../data/facebook-wosn-links/out.facebook-wosn-links.bestfrac",
		"../../data/facebook-wosn-wall/out.facebook-wosn-wall.bestfrac",
		"../../data/flickr-growth/out.flickr-growth.bestfrac",
		"../../data/link-dynamic-simplewiki/out.link-dynamic-simplewiki.bestfrac",
		"../../data/lkml-reply/out.lkml-reply.bestfrac",
		// "../../data/mit/out.mit.bestfrac",
		// "../../data/moreno_vdb/out.moreno_vdb_vdb.bestfrac",
		"../../data/munmun_digg_reply/out.munmun_digg_reply.bestfrac",
		"../../data/opsahl-ucsocial/out.opsahl-ucsocial.bestfrac",
		"../../data/prosper-loans/out.prosper-loans.bestfrac",
		// "../../data/radoslaw_email/out.radoslaw_email_email.bestfrac",
		"../../data/slashdot-threads/out.slashdot-threads.bestfrac",
		// "../../data/sociopatterns-hypertext/out.sociopatterns-hypertext.bestfrac",
		// "../../data/sociopatterns-infectious/out.sociopatterns-infectious.bestfrac",
		"../../data/topology/out.topology.bestfrac",
		"../../data/wikipedia-growth/out.wikipedia-growth.bestfrac",
		"../../data/wiki_talk_ar/out.wiki_talk_ar.bestfrac",
		"../../data/wiki_talk_de/out.wiki_talk_de.bestfrac",
		"../../data/wiki_talk_en/out.wiki_talk_en.bestfrac",
		"../../data/wiki_talk_es/out.wiki_talk_es.bestfrac",
		"../../data/wiki_talk_fr/out.wiki_talk_fr.bestfrac",
		"../../data/wiki_talk_it/out.wiki_talk_it.bestfrac",
		"../../data/wiki_talk_nl/out.wiki_talk_nl.bestfrac",
		"../../data/wiki_talk_pt/out.wiki_talk_pt.bestfrac",
		"../../data/wiki_talk_ru/out.wiki_talk_ru.bestfrac",
		"../../data/wiki_talk_zh/out.wiki_talk_zh.bestfrac"
	};

	std::vector<std::string> const FILENAMES_LARGE = {
		"../../data/flickr-growth/out.flickr-growth", // 12
		"../../data/wikipedia-growth/out.wikipedia-growth", // 25
		"../../data/wiki_talk_en/out.wiki_talk_en" // 28
	};

	std::vector<std::string> const FILENAMES_LARGE_BESTFRAC = {
		"../../data/flickr-growth/out.flickr-growth.bestfrac", // 12
		"../../data/wikipedia-growth/out.wikipedia-growth.bestfrac", // 25
		"../../data/wiki_talk_en/out.wiki_talk_en.bestfrac" // 28
	};

	std::vector<std::string> FILENAMES_COLLABORATION = {
		"../../data/../CollaborationGraphs/AuthorGraphs/graphs/wsdm.txt",
		"../../data/../CollaborationGraphs/AuthorGraphs/graphs/theory.txt",
		"../../data/../CollaborationGraphs/AuthorGraphs/graphs/web.txt",
	};
} // end anonymous namespace

//
// Unit Tests
//

namespace unit_tests { void testCompareDeterministicAndRandomized(); }

void unit_tests::testAll()
{
	// testIncrementalGraph();
	// testIncrementalKCenter();
	// testIncrementalKCenterRandomized(); // currently buggy, but not used anyway
	testCompareDeterministicAndRandomized();
}

void unit_tests::testIncrementalGraph()
{
	IncrementalGraph graph("../../data/youtube-u-growth/out.youtube-u-growth");

	// TEST(graph.numberOfNodes() == 3216075);
	// TEST(graph.numberOfEdges() == graph.numberOfNodes()-1);
	TEST(graph.hasNextEdge());

	std::size_t number_of_edges = graph.numberOfEdges();
	std::size_t number_of_future_edges = graph.numberOfFutureEdges();
	while (graph.hasNextEdge()) {
		auto new_edge = graph.addNextEdge();
		TEST(graph.getNeighborsOf(new_edge.id1).back() == new_edge.id2);
		TEST(graph.getNeighborsOf(new_edge.id2).back() == new_edge.id1);
		TEST(graph.numberOfEdges() == ++number_of_edges);
		TEST(graph.numberOfFutureEdges() == --number_of_future_edges);
	}
}

void unit_tests::testIncrementalKCenter()
{
	using UpdateAlgorithm = IncrementalKCenter::UpdateAlgorithm;

	std::vector<UpdateAlgorithm> algs = {
		// UpdateAlgorithm::Naive,
		// UpdateAlgorithm::NaiveSingleDist,
		// UpdateAlgorithm::Postfix,
		// UpdateAlgorithm::StarNode,
		UpdateAlgorithm::StarNodeSingleDist,
	};
	int k = 50;

	for (auto alg: algs) {
		IncrementalGraph graph("../../data/ca-cit-HepPh/out.ca-cit-HepPh");
		auto args = IncrementalKCenter::Arguments{alg, k, (double)k, 0};
		IncrementalKCenter ikcenter(graph, args);

		GraphAlgs graph_algs(graph);
		while (graph.hasNextEdge()) {
			auto new_edge = graph.addNextEdge();
			ikcenter.update(new_edge);

			if (graph.numberOfEdges()%1000 == 0 && ikcenter.hasValidClustering()) {
				Distances distances(graph.numberOfNodes());
				for (auto far_node_id: ikcenter.getCurrentFarNodes()) {
					graph_algs.freshBFS(far_node_id, distances);
					for (auto other: ikcenter.getCurrentFarNodes()) {
						if (other != far_node_id) {
							TEST(distances[other] >= ikcenter.currentKCenterDistance());
						}
					}
				}

				std::fill(distances.begin(), distances.end(), c::dist_max);
				for (auto center_node_id: ikcenter.getCurrentCenters()) {
					distances[center_node_id] = 0;
					graph_algs.updateBFS(center_node_id, distances);
				}

				for (auto node_id: graph.getNodeIDsRange()) {
					TEST(!ikcenter.isActive(node_id) ||
						 distances[node_id] <= ikcenter.currentKCenterDistance());
				}
			}
		}
	}
}

void unit_tests::testIncrementalKCenterRandomized()
{
	int k = 50;

	// connected
	{
		IncrementalGraph graph("../../data/connected.graph");
		IncrementalKCenterRandomized ikcenter(graph, k, .1);

		GraphAlgs graph_algs(graph);
		while (graph.hasNextEdge()) {
			auto new_edge = graph.addNextEdge();
			ikcenter.update(new_edge);

			if (graph.numberOfEdges()%1000 == 0) {
				Distances distances(graph.numberOfNodes());
				TEST((int)ikcenter.getCurrentFarNodes().size() == k+1);
				for (auto far_node_id: ikcenter.getCurrentFarNodes()) {
					graph_algs.freshBFS(far_node_id, distances);
					for (auto other: ikcenter.getCurrentFarNodes()) {
						if (other != far_node_id) {
							TEST(distances[other] >= ikcenter.currentKCenterDistance());
						}
					}
				}

				std::fill(distances.begin(), distances.end(), c::dist_max);
				TEST((int)ikcenter.getCurrentCenters().size() <= k);
				for (auto center_node_id: ikcenter.getCurrentCenters()) {
					distances[center_node_id] = 0;
					graph_algs.updateBFS(center_node_id, distances);
				}
				for (auto node_id: graph.getNodeIDsRange()) {
					TEST(distances[node_id] <= ikcenter.currentKCenterDistance());
				}
			}
		}
	}

	// general
	{
		IncrementalGraph graph("../../data/ca-cit-HepPh/out.ca-cit-HepPh");
		IncrementalKCenterRandomized ikcenter(graph, k, .1);

		GraphAlgs graph_algs(graph);
		while (graph.hasNextEdge()) {
			auto new_edge = graph.addNextEdge();
			ikcenter.update(new_edge);

			if (graph.numberOfEdges()%1000 == 0 && ikcenter.hasValidClustering()) {
				Distances distances(graph.numberOfNodes());
				for (auto far_node_id: ikcenter.getCurrentFarNodes()) {
					graph_algs.freshBFS(far_node_id, distances);
					for (auto other: ikcenter.getCurrentFarNodes()) {
						if (other != far_node_id) {
							TEST(distances[other] >= ikcenter.currentKCenterDistance());
						}
					}
				}

				std::fill(distances.begin(), distances.end(), c::dist_max);
				for (auto center_node_id: ikcenter.getCurrentCenters()) {
					distances[center_node_id] = 0;
					graph_algs.updateBFS(center_node_id, distances);
				}
				for (auto node_id: graph.getNodeIDsRange()) {
					TEST(!ikcenter.isActive(node_id) ||
						 distances[node_id] <= ikcenter.currentKCenterDistance());
				}
			}
		}
	}
}

void unit_tests::testCompareDeterministicAndRandomized()
{
	using UpdateAlgorithm = IncrementalKCenter::UpdateAlgorithm;

	int const k = 10;
	double const eps = .2;

	for (auto const& filename: FILENAMES) {
		std::cout << filename << std::endl;
		IncrementalGraph graph(filename);

		auto args1 = IncrementalKCenter::Arguments{UpdateAlgorithm::StarNodeSingleDist, k, (double)k, eps};
		IncrementalKCenter ikcenter(graph, args1);
		auto args2 = IncrementalKCenter::Arguments{UpdateAlgorithm::Randomized, k, (double)k, eps};
		IncrementalKCenter ikcenter_randomized(graph, args2);
		// IncrementalKCenterRandomized ikcenter_randomized(graph, k, eps);

		GraphAlgs graph_algs(graph);
		while (graph.hasNextEdge()) {
			auto new_edge = graph.addNextEdge();
			ikcenter.update(new_edge);
			ikcenter_randomized.update(new_edge);

			TEST(ikcenter.hasValidClustering() == ikcenter_randomized.hasValidClustering());

			if (ikcenter.hasValidClustering()) {
				auto d = ikcenter.currentKCenterDistance();
				auto dr = ikcenter_randomized.currentKCenterDistance();

				// TEST(d <= (2+eps)*dr && dr <= (2+eps)*d);
				if (!(d <= (2+eps)*dr && dr <= (2+eps)*d)) {
					{
						Distances distances(graph.numberOfNodes());
						for (auto far_node_id: ikcenter.getCurrentFarNodes()) {
							graph_algs.freshBFS(far_node_id, distances);
							for (auto other: ikcenter.getCurrentFarNodes()) {
								if (other != far_node_id) {
									TEST(distances[other] >= ikcenter.currentKCenterDistance());
								}
							}
						}

						std::fill(distances.begin(), distances.end(), c::dist_max);
						for (auto center_node_id: ikcenter.getCurrentCenters()) {
							distances[center_node_id] = 0;
							graph_algs.updateBFS(center_node_id, distances);
						}
						for (auto node_id: graph.getNodeIDsRange()) {
							TEST(!ikcenter.isActive(node_id) ||
								 distances[node_id] <= ikcenter.currentKCenterDistance());
						}
					}

					{
						Distances distances(graph.numberOfNodes());
						for (auto far_node_id: ikcenter_randomized.getCurrentFarNodes()) {
							graph_algs.freshBFS(far_node_id, distances);
							for (auto other: ikcenter_randomized.getCurrentFarNodes()) {
								if (other != far_node_id) {
									TEST(distances[other] >= ikcenter_randomized.currentKCenterDistance());
								}
							}
						}

						std::fill(distances.begin(), distances.end(), c::dist_max);
						for (auto center_node_id: ikcenter_randomized.getCurrentCenters()) {
							distances[center_node_id] = 0;
							graph_algs.updateBFS(center_node_id, distances);
						}
						for (auto node_id: graph.getNodeIDsRange()) {
							TEST(!ikcenter_randomized.isActive(node_id) ||
								 distances[node_id] <= ikcenter_randomized.currentKCenterDistance());
						}
					}

					// one of the above tests should fail...
					std::cout << d << " " << dr << std::endl;
					TEST(false);
				}
			}
		}
	}
}

// just in case anyone does anything stupid with this file...
#undef TEST
