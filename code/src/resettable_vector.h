#pragma once

#include <vector>

template <typename T>
class ResettableVector
{
public:
	using Index = std::size_t;
	using Value = T;

	ResettableVector(std::size_t size, Value default_value)
		: default_value(default_value), data(size, default_value) {}

	void set(Index index, Value value) {
		data[index] = value;
		to_reset.push_back(index);
	}

	Value operator[](Index index) {
		return data[index];
	}

	void reset() {
		for (auto index: to_reset) {
			data[index] = default_value;
		}
		to_reset.clear();
	}

private:
	Value const default_value;

	std::vector<Value> data;
	std::vector<Index> to_reset;
};
