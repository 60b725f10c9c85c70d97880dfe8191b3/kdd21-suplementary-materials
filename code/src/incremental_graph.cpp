#include "incremental_graph.h"

#include "union_find.h"

#include <algorithm>
#include <fstream>
#include <sstream>

IncrementalGraph::IncrementalGraph(std::string const& filename, bool connect_and_reduce)
{
	readGraph(filename, connect_and_reduce);
}

std::size_t IncrementalGraph::numberOfNodes() const
{
	return to_input_id.size();
}

std::size_t IncrementalGraph::numberOfEdges() const
{
	return number_of_edges;
}

std::size_t IncrementalGraph::numberOfFutureEdges() const
{
	return future_edges.size();
}

bool IncrementalGraph::hasNextEdge() const
{
	return !future_edges.empty();
}

Edge IncrementalGraph::addNextEdge()
{
	assert(!future_edges.empty());
	auto new_edge = future_edges.back();
	future_edges.pop_back();

	assert(isValidEdge(new_edge));

	neighbors_vec[new_edge.id1].push_back(new_edge.id2);
	neighbors_vec[new_edge.id2].push_back(new_edge.id1);
	++number_of_edges;

	return new_edge;
}

bool IncrementalGraph::addUnplannedEdge(Edge const& new_edge)
{
	assert(isValidEdge(new_edge));

	// only insert edge if it isn't in the graph yet
	auto const& id1_neighbors = neighbors_vec[new_edge.id1];
	auto it = std::find(id1_neighbors.begin(), id1_neighbors.end(), new_edge.id2);
	if (it == id1_neighbors.end()) {
		neighbors_vec[new_edge.id1].push_back(new_edge.id2);
		neighbors_vec[new_edge.id2].push_back(new_edge.id1);
		++number_of_edges;
		return true;
	}
	else {
		return false;
	}
}

ValueRange<NodeID> IncrementalGraph::getNodeIDsRange() const
{
	return ValueRange<NodeID>(0, numberOfNodes());
}

NodeIDs const& IncrementalGraph::getNeighborsOf(NodeID node_id) const
{
	return neighbors_vec[node_id];
}

NodeID IncrementalGraph::toInputID(NodeID node_id) const
{
	return to_input_id[node_id];
}

void IncrementalGraph::readGraph(std::string const& filename, bool connect_and_reduce)
{
	std::ifstream file(filename);
	if (!file.is_open()) {
		Error("The graph file couldn't be opened");
	}

	std::stringstream ss;
	ss << file.rdbuf();

	NodeIDs node_ids;

	auto ignore_count = std::numeric_limits<std::streamsize>::max();
	int throwaway;
	while (ss.peek(), !ss.eof()) {
		if (ss.peek() == '\n' || ss.peek() == '%') {
			ss.ignore(ignore_count, '\n');
			continue;
		}

		future_edges.emplace_back();
		auto& new_edge = future_edges.back();
		ss >> new_edge.id1 >> new_edge.id2 >> throwaway >> new_edge.time;

		// always have smaller id first
		if (new_edge.id1 > new_edge.id2) { std::swap(new_edge.id1, new_edge.id2); }
		// remove loops as they are annoying
		if (new_edge.id1 == new_edge.id2) {
			future_edges.pop_back();
		}
		else {
			node_ids.push_back(new_edge.id1);
			node_ids.push_back(new_edge.id2);
		}

		ss.ignore(ignore_count, '\n');
	}

	// deduplicate nodes
	{
		std::sort(node_ids.begin(), node_ids.end());
		auto new_end = std::unique(node_ids.begin(), node_ids.end());
		node_ids.erase(new_end, node_ids.end());
	}

	// deduplicate edges
	{
		auto id_lexicographic = [](Edge const& edge1, Edge const& edge2) {
			return edge1.id1 < edge2.id1 || (edge1.id1 == edge2.id1 && edge1.id2 < edge2.id2);
		};
		std::sort(future_edges.begin(), future_edges.end(), id_lexicographic);

		auto same_ids = [](Edge const& edge1, Edge const& edge2) {
			return edge1.id1 == edge2.id1 && edge1.id2 == edge2.id2;
		};
		auto new_end = std::unique(future_edges.begin(), future_edges.end(), same_ids);
		future_edges.erase(new_end, future_edges.end());

		// to not have effects on the input order of the edges, we randomize it
		std::random_device random_device;
		std::mt19937 random_generator(random_device());
		random_generator.seed(42); // fixed seed to still get consistency through different runs
		std::shuffle(future_edges.begin(), future_edges.end(), random_generator);
	}

	// sort decreasing according to timestamps
	auto later = [](Edge const& edge1, Edge const& edge2) {
		return edge1.time > edge2.time;
	};
	std::sort(future_edges.begin(), future_edges.end(), later);

	Edges edges_to_add;

	// add edges with -1 timestamp
	while (!future_edges.empty() && future_edges.back().time == -1) {
		edges_to_add.push_back(future_edges.back());
		future_edges.pop_back();
	}

	// preprocessing before finally building the graph
	if (connect_and_reduce) {
		greedilyConnectAndReduceGraph(node_ids, edges_to_add);
	}

	// giving contiguous IDs to nodes
	NodeIDs to_new_id(node_ids.back()+1);
	for (NodeID new_id = 0; new_id < node_ids.size(); ++new_id) {
		auto old_id = node_ids[new_id];
		to_new_id[old_id] = new_id;
	}

	to_input_id = node_ids;
	for (auto& edge: future_edges) {
		edge.id1 = to_new_id[edge.id1];
		edge.id2 = to_new_id[edge.id2];
	}
	for (auto& edge: edges_to_add) {
		edge.id1 = to_new_id[edge.id1];
		edge.id2 = to_new_id[edge.id2];
	}

	number_of_edges = 0;
	neighbors_vec.resize(numberOfNodes());

	// add edges that were chosen previously
	for (auto const& edge: edges_to_add) {
		addEdge(edge);
	}
}

bool IncrementalGraph::isValidEdge(Edge const& edge) const
{
	return edge.id1 < numberOfNodes() && edge.id2 < numberOfNodes();
}

void IncrementalGraph::addEdge(Edge const& edge)
{
	assert(isValidEdge(edge));

	neighbors_vec[edge.id1].push_back(edge.id2);
	neighbors_vec[edge.id2].push_back(edge.id1);
	++number_of_edges;
}

void IncrementalGraph::greedilyConnectAndReduceGraph(NodeIDs& node_ids, Edges& edges_to_add)
{
	UnionFind<NodeID> union_find;
	union_find.init(node_ids);

	// greedily add edges to connect the graph
	std::vector<bool> is_connecting_edge(future_edges.size(), false);
	for (std::size_t i = 0; i < future_edges.size(); ++i) {
		auto const& edge = future_edges[i];
		bool union_happened = union_find.addRelation({edge.id1, edge.id2});
		if (union_happened) {
			is_connecting_edge[i] = true;
		}
	}

	// reduce to largest connected component
	NodeIDs good_node_ids;
	for (auto node_id: node_ids) {
		if (union_find.belongsToLargestComponent(node_id)) {
			good_node_ids.push_back(node_id);
		}
	}
	node_ids.swap(good_node_ids);

	Edges good_edges;
	for (std::size_t i = 0; i < future_edges.size(); ++i) {
		auto const& edge = future_edges[i];
		auto id1_good = union_find.belongsToLargestComponent(edge.id1);
		auto id2_good = union_find.belongsToLargestComponent(edge.id2);
		if (id1_good && id2_good) {
			if (is_connecting_edge[i]) {
				edges_to_add.push_back(edge);
			}
			else {
				good_edges.push_back(edge);
			}
		}
	}
	future_edges.swap(good_edges);
}
