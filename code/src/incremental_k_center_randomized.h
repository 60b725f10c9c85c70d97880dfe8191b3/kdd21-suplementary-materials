#pragma once

#include "basic_types.h"
#include "graph_algs.h"
#include "incremental_graph.h"
#include "union_find.h"
#include "random.h"

#include <cmath>

// NOTES:
// - This class should only be used with connected graphs!

namespace unit_tests { void testIncrementalKCenterRandomized(); }

class IncrementalKCenterRandomized
{
public:
	IncrementalKCenterRandomized(IncrementalGraph const& graph, int const k, double const epsilon);

	void update(Edge const& edge);
	bool farNodesChangedLastUpdate() const;

	bool hasValidClustering() const;
	NodeIDs getCurrentCenters() const;
	NodeIDs getCurrentFarNodes() const;
	Distance currentKCenterDistance() const;
	NodeIDs const& getActiveNodes() const;
	bool isActive(NodeID node_id) const;

	std::size_t getFarNodeUpdateCount() const;

private:
	IncrementalGraph const& graph;
	int const k;
	double const q;
	double const epsilon;
	std::size_t const component_threshold;

	GraphAlgs graph_algs;
	Random random;
	Distance min_distance, max_distance;
	std::size_t center_updates = 0;
	bool far_nodes_changed_last_update;

	// component related members
	UnionFind<NodeID> union_find;
	// This contains all the nodes that are in a large component, i.e., a component
	// that has size at least "component_threshold".
	NodeIDs active_nodes;
	std::vector<bool> is_active;
	std::size_t number_of_active_components;

	struct Bucket {
		std::size_t index;
		Distance distance;
		NodeIDs centers;
		Distances distances;
		std::vector<std::size_t> closest_center;
		std::size_t number_of_far_nodes;
	};
	using Buckets = std::vector<Bucket>;

	Buckets buckets;
	NodeIDs boundary;
	ResettableVector<bool> in_boundary;

	void init();
	void initComponents();
	void initialGonzalez(Bucket& bucket);
	void incrementalGonzalez(Bucket& bucket, bool count_updates = true);
	std::size_t updateDistances(Bucket& bucket, Edge const& edge);
	void updateFarNodes(Bucket& bucket, std::size_t delete_index);
	void updateComponents(NodeID node_id1, NodeID node_id2);
	void addComponentToActiveNodes(NodeID start_id);
	bool introducedFarNode(Bucket& bucket, Edge const& edge);

	bool isClustering(Bucket const& b) const { return (int)b.centers.size() <= k; }
	bool isDispersion(Bucket const& b) const { return (int)b.centers.size() == k+1; }

	// TODO: for debugging
	void checkDistances(std::string const& s = "");
	void checkDistances(Bucket const& bucket, std::string const& s = "");
};
