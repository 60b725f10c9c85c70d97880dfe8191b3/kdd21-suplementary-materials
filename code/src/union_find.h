#pragma once

#include "random.h"

#include <unordered_map>
#include <vector>

template <typename T>
class UnionFind
{
public:
	using Relation = std::pair<T, T>;

	UnionFind() = default;

	void init(std::size_t number_of_elements);
	void init(std::vector<T> const& base_set);

	// Returns "true" if a union happens.
	bool addRelation(Relation relation);
	bool belongToSameComponent(T value1, T value2);
	bool belongsToLargestComponent(T value);

	std::size_t getComponentSize(T value);

	std::size_t sizeOfLargestComponent() const;
	std::size_t IdOfLargestComponent() const;
	std::size_t getNumberOfComponents() const;

private:
	// data strucutres
	using ElementID = int64_t;
	static ElementID const NO_ID = -1;

	struct Element
	{
		ElementID const id;
		T const value;

		ElementID parent_id;
		std::size_t tree_size;

		Element(ElementID id, T value)
			: id(id), value(value), parent_id(NO_ID), tree_size(1) {}
	};

	std::unordered_map<T, ElementID> to_id;
	std::vector<Element> elements;
	Random random;

	ElementID max_id;
	std::size_t max_size;
	std::size_t number_of_components;

	// helper functions
	void initDataStructures(std::vector<T> const& base_set);
	ElementID findRoot(ElementID id);
	void uniteSets(ElementID root1, ElementID root2);
};

template <typename T>
void UnionFind<T>::init(std::size_t number_of_elements)
{
	if (number_of_elements == 0) { return; }

	// TODO: This is too inefficient, but not a bottleneck. Only fix if necessary.
	std::vector<T> base_set(number_of_elements);
	std::iota(base_set.begin(), base_set.end(), 0);

	initDataStructures(base_set);
}

template <typename T>
void UnionFind<T>::init(std::vector<T> const& base_set)
{
	if (base_set.empty()) { return; }

	initDataStructures(base_set);
}

template <typename T>
bool UnionFind<T>::addRelation(Relation relation)
{
	auto root1 = findRoot(to_id[relation.first]);
	auto root2 = findRoot(to_id[relation.second]);

	if (root1 != root2) {
		uniteSets(root1, root2);
		return true;
	}

	return false;
}

template <typename T>
bool UnionFind<T>::belongToSameComponent(T value1, T value2)
{
	auto root1 = findRoot(to_id[value1]);
	auto root2 = findRoot(to_id[value2]);
	return root1 == root2;
}

template <typename T>
bool UnionFind<T>::belongsToLargestComponent(T value)
{
	return findRoot(to_id[value]) == max_id;
}

template <typename T>
std::size_t UnionFind<T>::getComponentSize(T value)
{
	return elements[findRoot(to_id[value])].tree_size;
}

template <typename T>
std::size_t UnionFind<T>::sizeOfLargestComponent() const
{
	return max_size;
}

template <typename T>
std::size_t UnionFind<T>::getNumberOfComponents() const
{
	return number_of_components;
}

template <typename T>
void UnionFind<T>::initDataStructures(std::vector<T> const& base_set)
{
	ElementID id = 0;
	for (auto value: base_set) {
		elements.emplace_back(id, value);
		to_id.emplace(value, id);

		++id;
	}

	max_id = 0;
	max_size = 1;
	number_of_components = elements.size();
}

template <typename T>
auto UnionFind<T>::findRoot(ElementID const id) -> ElementID
{
	// find root
	ElementID current_id = id;
	while (elements[current_id].parent_id != NO_ID) {
		current_id = elements[current_id].parent_id;
	}

	auto root = current_id;

	// path compression
	current_id = id;
	while (current_id != root) {
		auto parent_id = elements[current_id].parent_id;
		elements[current_id].parent_id = root;
		current_id = parent_id;
	}

	return root;
}

template <typename T>
void UnionFind<T>::uniteSets(ElementID root1_id, ElementID root2_id)
{
	auto& root1 = elements[root1_id];
	auto& root2 = elements[root2_id];

	// use randomization to decide which tree is hung under which
	if (random.throwCoin()) {
		root1.parent_id = root2_id;
		root2.tree_size += root1.tree_size;

		if (root2.tree_size > max_size) {
			max_id = root2_id;
			max_size = root2.tree_size;
		}
	}
	else {
		root2.parent_id = root1_id;
		root1.tree_size += root2.tree_size;

		if (root1.tree_size > max_size) {
			max_id = root1_id;
			max_size = root1.tree_size;
		}
	}

	--number_of_components;
}
