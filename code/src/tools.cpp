#include "defs.h"
#include "incremental_graph.h"
#include "union_find.h"

#include <algorithm>
#include <iostream>
#include <fstream>
#include <set>

std::size_t tool1(std::string const& filename, double frac)
{
	IncrementalGraph graph(filename);

	NodeIDs node_ids;
	for (auto node_id: graph.getNodeIDsRange()) {
		node_ids.push_back(node_id);
	}

	UnionFind<NodeID> uf;
	uf.init(node_ids);

	while (graph.hasNextEdge()) {
		auto edge = graph.addNextEdge();
		uf.addRelation({edge.id1, edge.id2});

		if (uf.sizeOfLargestComponent() > graph.numberOfNodes()/frac) {
			break;
		}
	}

	std::size_t remaining_edges = graph.numberOfFutureEdges();
	std::vector<bool> in_component(graph.numberOfNodes(), false);
	for (auto node_id: graph.getNodeIDsRange()) {
		in_component[node_id] = uf.belongsToLargestComponent(node_id);
	}

	std::size_t count = 0;
	while (graph.hasNextEdge()) {
		auto edge = graph.addNextEdge();
		if (in_component[edge.id1] && in_component[edge.id2]) {
			++count;
		}
	}

	std::cout << "Graph: " << filename << " (" << graph.numberOfEdges() << ")" << std::endl;
	std::cout << "Number of valid edges after becoming connected: " << count << std::endl;
	std::cout << "Fraction of edges that belong to the component: " << (double)count/remaining_edges << std::endl;

	return count;
}

void exportGraph(std::string const& filename, double frac, std::string const& new_graph_filename)
{
	IncrementalGraph graph(filename);

	NodeIDs node_ids;
	for (auto node_id: graph.getNodeIDsRange()) {
		node_ids.push_back(node_id);
	}

	UnionFind<NodeID> uf;
	uf.init(node_ids);

	while (graph.hasNextEdge()) {
		auto edge = graph.addNextEdge();
		uf.addRelation({edge.id1, edge.id2});

		if (uf.sizeOfLargestComponent() > graph.numberOfNodes()/frac) {
			break;
		}
	}

	Edges edges;

	std::vector<bool> in_component(graph.numberOfNodes(), false);
	for (auto node_id: graph.getNodeIDsRange()) {
		in_component[node_id] = uf.belongsToLargestComponent(node_id);
		node_ids.push_back(node_id);
	}

	std::set<Edge> dynamic_edges;
	while (graph.hasNextEdge()) {
		auto edge = graph.addNextEdge();
		if (in_component[edge.id1] && in_component[edge.id2]) {
			dynamic_edges.insert(edge);
		}
	}

	IncrementalGraph fresh_graph(filename);

	while (fresh_graph.hasNextEdge()) {
		auto edge = fresh_graph.addNextEdge();
		if (in_component[edge.id1] && in_component[edge.id2]) {
			if (dynamic_edges.count(edge) == 0) {
				edge.time = -1;
			}
			edges.push_back(edge);
		}
	}

	// write graph
	std::ofstream f(new_graph_filename);
	auto const throwaway = 0;
	for (auto const& edge: edges) {
		f << edge.id1 << " " << edge.id2 << " " << throwaway << " " << edge.time << "\n";
	}
}

int main(int argc, char* argv[])
{
	std::vector<std::string> filenames = {
		"../../data/ca-cit-HepPh/out.ca-cit-HepPh",
		"../../data/ca-cit-HepTh/out.ca-cit-HepTh",
		"../../data/chess/out.chess",
		"../../data/contact/out.contact",
		"../../data/dblp_coauthor/out.dblp_coauthor",
		"../../data/digg-friends/out.digg-friends",
		"../../data/dnc-temporalGraph/out.dnc-temporalGraph",
		"../../data/elec/out.elec",
		"../../data/enron/out.enron",
		"../../data/epinions/out.epinions",
		"../../data/facebook-wosn-links/out.facebook-wosn-links",
		"../../data/facebook-wosn-wall/out.facebook-wosn-wall",
		"../../data/flickr-growth/out.flickr-growth",
		"../../data/link-dynamic-simplewiki/out.link-dynamic-simplewiki",
		"../../data/lkml-reply/out.lkml-reply",
		"../../data/mit/out.mit",
		"../../data/moreno_vdb/out.moreno_vdb_vdb",
		"../../data/munmun_digg_reply/out.munmun_digg_reply",
		"../../data/opsahl-ucsocial/out.opsahl-ucsocial",
		"../../data/prosper-loans/out.prosper-loans",
		"../../data/radoslaw_email/out.radoslaw_email_email",
		"../../data/slashdot-threads/out.slashdot-threads",
		"../../data/sociopatterns-hypertext/out.sociopatterns-hypertext",
		"../../data/sociopatterns-infectious/out.sociopatterns-infectious",
		"../../data/topology/out.topology",
		"../../data/wikipedia-growth/out.wikipedia-growth",
		"../../data/wiki_talk_ar/out.wiki_talk_ar",
		"../../data/wiki_talk_de/out.wiki_talk_de",
		"../../data/wiki_talk_en/out.wiki_talk_en",
		"../../data/wiki_talk_es/out.wiki_talk_es",
		"../../data/wiki_talk_fr/out.wiki_talk_fr",
		"../../data/wiki_talk_it/out.wiki_talk_it",
		"../../data/wiki_talk_nl/out.wiki_talk_nl",
		"../../data/wiki_talk_pt/out.wiki_talk_pt",
		"../../data/wiki_talk_ru/out.wiki_talk_ru",
		"../../data/wiki_talk_zh/out.wiki_talk_zh",
	};

	std::vector<double> fracs = {16., 8., 4., 2., 1.75, 1.5, 1.3, 1.1, 1.05, 1.01};

	for (auto const& filename: filenames) {
		std::cout << "FILENAME: " << filename << std::endl;

		std::vector<std::size_t> counts;
		for (auto frac: fracs) {
			std::cout << "FRAC: " << frac << std::endl;
			counts.push_back(tool1(filename, frac));
		}

		auto const max_it = std::max_element(counts.begin(), counts.end());
		double const best_frac = fracs[std::distance(counts.begin(), max_it)];

		exportGraph(filename, best_frac, filename + ".bestfrac");
	}
}


