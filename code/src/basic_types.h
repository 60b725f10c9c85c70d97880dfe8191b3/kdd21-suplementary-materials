#pragma once

#include "id.h"

#include <limits>
#include <vector>
#include <cstdint>

struct Node {};

using NodeID = ID<Node>;
using NodeIDs = std::vector<NodeID>;

using Timestap = int64_t;

struct Edge {
	NodeID id1, id2;
	Timestap time;

	Edge() = default;
	Edge(NodeID id1, NodeID id2, Timestap time) : id1(id1), id2(id2), time(time) {}

	bool operator<(Edge const& other) const {
		return id1 < other.id1 ||
			(id1 == other.id1 && id2 < other.id2) ||
			(id1 == other.id1 && id2 == other.id2 && time < other.time);
	}
};
using Edges = std::vector<Edge>;
using EdgeID = std::size_t;
using EdgeIDs = std::vector<EdgeID>;

using Distance = float;
using Distances = std::vector<Distance>;

namespace c {

Distance const dist_max = std::numeric_limits<Distance>::max();
std::size_t const invalid_index = std::numeric_limits<std::size_t>::max();

}
