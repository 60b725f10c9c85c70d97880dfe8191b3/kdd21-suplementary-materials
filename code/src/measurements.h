// To add a new measurement, add a new enum value to MEASUREMENT::EXP. This will
// be the handle to the measurement. Then you also have to add a tuple of the
// form (<name>, <handle>, <type>). The values mean the following:
//
// <name>: This is just a string that will be used when printing.
//
// <handle>: The handle which is used to reference the measurment in the code. This
//           has to be the same that was also added to the enum.
//
// <type>: There are serveral types of measurements.
//     * TIMER: Measures time using a start and stop function. Afterwards the sum
//              and mean can be calculated.
//     * INT: Accumulating int values. The sum and mean can be recalled from that.
//     * FLOAT: Same as INT just for float values.
//     * TIMER_DATA: Like TIMER but all the measurements are collected and thus
//                   more complex stats can be created like standard deviation.
//     * INT_DATA: Like TIMER_DATA but for INT.
//     * FLOAT_DATA: Like TIMER_DATA but for FLOAT.
//     * COUNTER: A simple counter which can just be increased.


// Add new measurements here ...
enum class MEASUREMENT::EXP {
	DummyTimer1,
	DummyTimer2,
	DummyTimer3,
	DummyTimer4,
	DummyTimer5,
	// DummyCounter1,
	// DummyCounter2,
	// DummyCounter3,
	// DummyCounter4,
	// DummyCounter5,
	ReadGraph,
	InitKCenter,
	AddEdge,
	KCenterUpdate,
	UpdateComponents,
	UpdateDistances,
	UpdateFarNodes,
	IntroducedFarNode,
	GonzalezAfterNodeGotFurther,
	GonzalezForNewValidClustering,
	AlgorithmTotal,
};

inline auto MEASUREMENT::getMeasurements() -> Measurements {
// ... and here
	return {
		{"dummy timer 1", EXP::DummyTimer1, TIMER},
		{"dummy timer 2", EXP::DummyTimer2, TIMER},
		{"dummy timer 3", EXP::DummyTimer3, TIMER},
		{"dummy timer 4", EXP::DummyTimer4, TIMER},
		{"dummy timer 5", EXP::DummyTimer5, TIMER},
		// {"dummy counter 1", EXP::DummyCounter1, COUNTER},
		// {"dummy counter 2", EXP::DummyCounter2, COUNTER},
		// {"dummy counter 3", EXP::DummyCounter3, COUNTER},
		// {"dummy counter 4", EXP::DummyCounter4, COUNTER},
		// {"dummy counter 5", EXP::DummyCounter5, COUNTER},
		{"reading graph timer", EXP::ReadGraph, TIMER},
		{"k-center initialization", EXP::InitKCenter, TIMER},
		{"adding edges to the dynamic graph", EXP::AddEdge, TIMER},
		{"updating the k-center", EXP::KCenterUpdate, TIMER},
		{"component update after edge insertion", EXP::UpdateComponents, TIMER},
		{"distances update after edge insertion", EXP::UpdateDistances, TIMER},
		{"updating the far nodes after edge insertion", EXP::UpdateFarNodes, TIMER},
		{"check for introduction of far node after components merge", EXP::IntroducedFarNode, TIMER},
		{"recompute clustering from scratch after components merged", EXP::GonzalezAfterNodeGotFurther, TIMER},
		{"recompute clustering from scratch after it became valid", EXP::GonzalezForNewValidClustering, TIMER},
		{"TOTAL (after initialization): ", EXP::AlgorithmTotal, TIMER},
	};
}
