#include "basic_types.h"
#include "defs.h"
#include "incremental_graph.h"
#include "incremental_k_center.h"
#include "incremental_k_center_randomized.h"
#include "measurement_tool.h"
#include "random.h"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <tuple>

void printUsage()
{
	std::cout << "Usage: ./experiments <experiment_id>\n";
	std::cout << "For possible experiment ids check src/experiments.cpp.\n";
}

namespace
{
	std::vector<std::string> const FILENAMES = {
		"../../data/ca-cit-HepPh/out.ca-cit-HepPh",
		"../../data/ca-cit-HepTh/out.ca-cit-HepTh",
		"../../data/chess/out.chess",
		// "../../data/contact/out.contact", // small
		"../../data/dblp_coauthor/out.dblp_coauthor",
		"../../data/digg-friends/out.digg-friends",
		"../../data/dnc-temporalGraph/out.dnc-temporalGraph",
		"../../data/elec/out.elec",
		"../../data/enron/out.enron",
		"../../data/epinions/out.epinions",
		"../../data/facebook-wosn-links/out.facebook-wosn-links",
		"../../data/facebook-wosn-wall/out.facebook-wosn-wall",
		"../../data/flickr-growth/out.flickr-growth", // 12
		"../../data/link-dynamic-simplewiki/out.link-dynamic-simplewiki",
		"../../data/lkml-reply/out.lkml-reply",
		// "../../data/mit/out.mit", // small
		// "../../data/moreno_vdb/out.moreno_vdb_vdb", // small
		"../../data/munmun_digg_reply/out.munmun_digg_reply",
		"../../data/opsahl-ucsocial/out.opsahl-ucsocial",
		"../../data/prosper-loans/out.prosper-loans",
		// "../../data/radoslaw_email/out.radoslaw_email_email", // small
		"../../data/slashdot-threads/out.slashdot-threads",
		// // "../../data/sociopatterns-hypertext/out.sociopatterns-hypertext", // small
		// "../../data/sociopatterns-infectious/out.sociopatterns-infectious", // small
		"../../data/topology/out.topology",
		"../../data/wikipedia-growth/out.wikipedia-growth", // 25
		"../../data/wiki_talk_ar/out.wiki_talk_ar",
		"../../data/wiki_talk_de/out.wiki_talk_de",
		"../../data/wiki_talk_en/out.wiki_talk_en", // 28
		"../../data/wiki_talk_es/out.wiki_talk_es",
		"../../data/wiki_talk_fr/out.wiki_talk_fr",
		"../../data/wiki_talk_it/out.wiki_talk_it",
		"../../data/wiki_talk_nl/out.wiki_talk_nl",
		"../../data/wiki_talk_pt/out.wiki_talk_pt",
		"../../data/wiki_talk_ru/out.wiki_talk_ru",
		"../../data/wiki_talk_zh/out.wiki_talk_zh"
	};

	std::vector<std::string> const FILENAMES_BESTFRAC = {
		"../../data/ca-cit-HepPh/out.ca-cit-HepPh.bestfrac",
		"../../data/ca-cit-HepTh/out.ca-cit-HepTh.bestfrac",
		"../../data/chess/out.chess.bestfrac",
		// "../../data/contact/out.contact.bestfrac",
		"../../data/dblp_coauthor/out.dblp_coauthor.bestfrac",
		"../../data/digg-friends/out.digg-friends.bestfrac",
		"../../data/dnc-temporalGraph/out.dnc-temporalGraph.bestfrac",
		"../../data/elec/out.elec.bestfrac",
		"../../data/enron/out.enron.bestfrac",
		"../../data/epinions/out.epinions.bestfrac",
		"../../data/facebook-wosn-links/out.facebook-wosn-links.bestfrac",
		"../../data/facebook-wosn-wall/out.facebook-wosn-wall.bestfrac",
		"../../data/flickr-growth/out.flickr-growth.bestfrac",
		"../../data/link-dynamic-simplewiki/out.link-dynamic-simplewiki.bestfrac",
		"../../data/lkml-reply/out.lkml-reply.bestfrac",
		// "../../data/mit/out.mit.bestfrac",
		// "../../data/moreno_vdb/out.moreno_vdb_vdb.bestfrac",
		"../../data/munmun_digg_reply/out.munmun_digg_reply.bestfrac",
		"../../data/opsahl-ucsocial/out.opsahl-ucsocial.bestfrac",
		"../../data/prosper-loans/out.prosper-loans.bestfrac",
		// "../../data/radoslaw_email/out.radoslaw_email_email.bestfrac",
		"../../data/slashdot-threads/out.slashdot-threads.bestfrac",
		// "../../data/sociopatterns-hypertext/out.sociopatterns-hypertext.bestfrac",
		// "../../data/sociopatterns-infectious/out.sociopatterns-infectious.bestfrac",
		"../../data/topology/out.topology.bestfrac",
		"../../data/wikipedia-growth/out.wikipedia-growth.bestfrac",
		"../../data/wiki_talk_ar/out.wiki_talk_ar.bestfrac",
		"../../data/wiki_talk_de/out.wiki_talk_de.bestfrac",
		"../../data/wiki_talk_en/out.wiki_talk_en.bestfrac",
		"../../data/wiki_talk_es/out.wiki_talk_es.bestfrac",
		"../../data/wiki_talk_fr/out.wiki_talk_fr.bestfrac",
		"../../data/wiki_talk_it/out.wiki_talk_it.bestfrac",
		"../../data/wiki_talk_nl/out.wiki_talk_nl.bestfrac",
		"../../data/wiki_talk_pt/out.wiki_talk_pt.bestfrac",
		"../../data/wiki_talk_ru/out.wiki_talk_ru.bestfrac",
		"../../data/wiki_talk_zh/out.wiki_talk_zh.bestfrac"
	};

	std::vector<std::string> const FILENAMES_LARGE = {
		"../../data/flickr-growth/out.flickr-growth", // 12
		"../../data/wikipedia-growth/out.wikipedia-growth", // 25
		"../../data/wiki_talk_en/out.wiki_talk_en" // 28
	};

	std::vector<std::string> const FILENAMES_LARGE_BESTFRAC = {
		"../../data/flickr-growth/out.flickr-growth.bestfrac", // 12
		"../../data/wikipedia-growth/out.wikipedia-growth.bestfrac", // 25
		"../../data/wiki_talk_en/out.wiki_talk_en.bestfrac" // 28
	};

	std::vector<std::string> FILENAMES_COLLABORATION = {
		"../../data/../CollaborationGraphs/AuthorGraphs/graphs/wsdm.txt",
		"../../data/../CollaborationGraphs/AuthorGraphs/graphs/theory.txt",
		"../../data/../CollaborationGraphs/AuthorGraphs/graphs/web.txt",
	};
} // end anonymous namespace

using UpdateAlgorithm = IncrementalKCenter::UpdateAlgorithm;

// TODO: Actually, we could just read and preprocess the graph once and then reset
// after each usage, but this is not implemented yet.
void run(std::string const& filename, IncrementalKCenter::Arguments const& args)
{
	MEASUREMENT::reset();

	// Reading the graph and initializing the incremental k-center clustering algorithm
	std::cout << "Reading and processing input graph." << std::endl;
	MEASUREMENT::start(EXP::ReadGraph);
	IncrementalGraph graph(filename);
	MEASUREMENT::stop(EXP::ReadGraph);

	std::cout << "Initializing the incremental k-center algorithm." << std::endl;
	MEASUREMENT::start(EXP::InitKCenter);
	IncrementalKCenter ikcenter(graph, args);
	MEASUREMENT::stop(EXP::InitKCenter);

	std::cout << "Starting to run the incremental k-center algorithm." << std::endl;
	std::size_t round_update_count = 0;
	MEASUREMENT::start(EXP::AlgorithmTotal);
	while (graph.hasNextEdge()) {
		MEASUREMENT::start(EXP::AddEdge);
		auto new_edge = graph.addNextEdge();
		MEASUREMENT::stop(EXP::AddEdge);

		MEASUREMENT::start(EXP::KCenterUpdate);
		ikcenter.update(new_edge);
		MEASUREMENT::stop(EXP::KCenterUpdate);

		if (ikcenter.farNodesChangedLastUpdate()) {
			++round_update_count;
		}
	}
	MEASUREMENT::stop(EXP::AlgorithmTotal);
	std::cout << std::endl;

	std::cout << "Number of rounds in which the far nodes changed: " << round_update_count << std::endl;
	std::cout << "Number of far nodes changed: " << ikcenter.getFarNodeUpdateCount() << std::endl;
	std::cout << "Number of far nodes changed on decrement: "
		<< ikcenter.getDecFarNodeUpdateCount() << std::endl;
	std::cout << "Number of far nodes changed on increment: "
		<< ikcenter.getIncFarNodeUpdateCount() << std::endl;

	MEASUREMENT::print();
}

void experiment1() 
{
	auto filenames = {
		"../../data/ca-cit-HepPh/out.ca-cit-HepPh",
		"../../data/facebook-wosn-links/out.facebook-wosn-links",
		"../../data/youtube-u-growth/out.youtube-u-growth",
		"../../data/wikipedia-growth/out.wikipedia-growth",
		"../../data/../CollaborationGraphs/AuthorGraphs/graphs/wsdm.txt",
		"../../data/../CollaborationGraphs/AuthorGraphs/graphs/theory.txt",
		"../../data/../CollaborationGraphs/AuthorGraphs/graphs/web.txt",
	};
	auto ks = {5, 20, 50};
	auto algs = {
		// UpdateAlgorithm::Naive,
		// UpdateAlgorithm::NaiveSingleDist,
		// UpdateAlgorithm::Postfix,
		// UpdateAlgorithm::StarNode,
		UpdateAlgorithm::StarNodeSingleDist,
	};

	for (auto const& filename: filenames) {
		for (auto k: ks) {
			for (auto alg: algs) {
				std::cout << "\n\n";
				std::cout << "Running experiment with:\n";
				std::cout << "graph: " << filename << "\n";
				std::cout << "k: " << k << "\n";
				std::cout << "algorithm: " << IncrementalKCenter::toString(alg) << "\n";
				std::cout << std::endl;

				auto args = IncrementalKCenter::Arguments{alg, k, (double)50, .1};
				run(filename, args);
			}
		}
	}
}

std::size_t setDifferenceSize(NodeIDs old_ids, NodeIDs new_ids)
{
	NodeIDs difference;

	std::sort(old_ids.begin(), old_ids.end());
	std::sort(new_ids.begin(), new_ids.end());
	std::set_difference(old_ids.begin(), old_ids.end(), new_ids.begin(), new_ids.end(), std::back_inserter(difference));
	return difference.size();
}

std::tuple<std::size_t, std::size_t> run(IncrementalGraph& graph, IncrementalKCenterRandomized& ikcenter)
{
	std::cout << "Starting to run the incremental k-center theory algorithm." << std::endl;
	MEASUREMENT::start(EXP::AlgorithmTotal);
	std::size_t round_update_count = 0;
	std::size_t far_nodes_change_count = 0;
	NodeIDs far_nodes = ikcenter.getCurrentFarNodes();
	while (graph.hasNextEdge()) {
		MEASUREMENT::start(EXP::AddEdge);
		auto new_edge = graph.addNextEdge();
		MEASUREMENT::stop(EXP::AddEdge);

		MEASUREMENT::start(EXP::KCenterUpdate);
		ikcenter.update(new_edge);
		MEASUREMENT::stop(EXP::KCenterUpdate);

		if (ikcenter.farNodesChangedLastUpdate()) {
			++round_update_count;
			far_nodes_change_count += setDifferenceSize(far_nodes, ikcenter.getCurrentFarNodes());
			far_nodes = ikcenter.getCurrentFarNodes();
		}
	}
	MEASUREMENT::stop(EXP::AlgorithmTotal);

	return std::make_tuple(round_update_count, far_nodes_change_count);
}

std::tuple<std::size_t, std::size_t> run(IncrementalGraph& graph, IncrementalKCenter& ikcenter)
{
	MEASUREMENT::start(EXP::AlgorithmTotal);
	// std::cout << "Starting to run the incremental k-center algorithm." << std::endl;
	std::size_t round_update_count = 0;
	std::size_t far_nodes_change_count = 0;

	NodeIDs far_nodes = ikcenter.hasValidClustering() ? ikcenter.getCurrentFarNodes() : NodeIDs();

	while (graph.hasNextEdge()) {
		MEASUREMENT::start(EXP::AddEdge);
		auto new_edge = graph.addNextEdge();
		MEASUREMENT::stop(EXP::AddEdge);

		MEASUREMENT::start(EXP::KCenterUpdate);
		ikcenter.update(new_edge);
		MEASUREMENT::stop(EXP::KCenterUpdate);

		if (!ikcenter.hasValidClustering()) { far_nodes.clear(); continue; }

		// TODO: this is also timed -- check how costly that is in total
		if (ikcenter.farNodesChangedLastUpdate()) {
			++round_update_count;
			far_nodes_change_count += setDifferenceSize(far_nodes, ikcenter.getCurrentFarNodes());
			far_nodes = ikcenter.getCurrentFarNodes();
		}
	}
	MEASUREMENT::stop(EXP::AlgorithmTotal);

	return std::make_tuple(round_update_count, far_nodes_change_count);
}

void runTheoretical(std::string const& filename, int k, double epsilon)
{
	std::size_t number_of_randomized_runs = 10;
	double round_update_count = 0;
	double far_node_update_count = 0;
	double far_node_update_count_precise = 0;

	for (std::size_t i = 0; i < number_of_randomized_runs; ++i) {
		IncrementalGraph graph(filename);
		IncrementalKCenterRandomized ikcenter(graph, k, epsilon);

		std::size_t round_update_count_tmp, far_node_update_count_precise_tmp;
		std::tie(round_update_count_tmp, far_node_update_count_precise_tmp)
			= run(graph, ikcenter);


		round_update_count += round_update_count_tmp;
		far_node_update_count += ikcenter.getFarNodeUpdateCount();
		far_node_update_count_precise += far_node_update_count_precise_tmp;
	}

	std::cout << "Number of rounds in which the far nodes changed: "
		<< round_update_count/number_of_randomized_runs << std::endl;
	std::cout << "Number of far nodes changed: "
		<< far_node_update_count/number_of_randomized_runs << std::endl;
	std::cout << "Number of far nodes changed precise: "
		<< far_node_update_count_precise/number_of_randomized_runs << std::endl;
	MEASUREMENT::print(); // Note: this prints measurements that are summed over all runs!
	MEASUREMENT::reset();
}

struct PracticalResult
{
	double far_node_update_count_precise;
};

PracticalResult runPractical(std::string const& filename, int k, double epsilon,
                             UpdateAlgorithm alg = UpdateAlgorithm::StarNodeSingleDist, bool non_naive_gonzalez = true)
{
	double const q = 10;
	double round_update_count, far_node_update_count_precise;

	IncrementalGraph graph(filename);
	auto args =
		IncrementalKCenter::Arguments{alg, k, (double)q, epsilon};
	IncrementalKCenter ikcenter(graph, args);
	ikcenter.useNonNaiveGonzalez(non_naive_gonzalez);
	std::tie(round_update_count, far_node_update_count_precise) = run(graph, ikcenter);

	std::cout << "Number of rounds in which the far nodes changed: " << round_update_count << std::endl;
	std::cout << "Number of far nodes changed: " << ikcenter.getFarNodeUpdateCount() << std::endl;
	std::cout << "Number of far nodes changed on decrement: "
		<< ikcenter.getDecFarNodeUpdateCount() << std::endl;
	std::cout << "Number of far nodes changed on increment: "
		<< ikcenter.getIncFarNodeUpdateCount() << std::endl;
	std::cout << "Number of far nodes changed precise: " << far_node_update_count_precise << std::endl;
	MEASUREMENT::print();
	MEASUREMENT::reset();

	return PracticalResult{far_node_update_count_precise};
}

void compare_theory_and_pratical(bool run_theoretical, bool run_practical)
{
	// std::vector<int> ks = {5, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500};
	std::vector<int> ks = {4, 49, 99, 149, 199, 249, 299, 349, 399, 449, 499}; // in the paper, k is the dispersion and not clustering value
	// auto ks = {10};
	// auto epsilons = {.5, .2, .1, .05, .01};
	auto epsilons = {.1};

	for (auto const& filename: FILENAMES_BESTFRAC) {
		// don't consider graphs with too few nodes
		IncrementalGraph graph(filename);
		if ((int)graph.numberOfNodes() < ks.back()) { continue; }

		for (auto k: ks) {
			for (double epsilon: epsilons) {
				std::cout << "\n\n";
				std::cout << "Running experiment with:\n";
				std::cout << "graph: " << filename << "\n";
				std::cout << "k: " << k << "\n";
				std::cout << "epsilon: " << epsilon << "\n";
				std::cout << std::endl;

				if (run_theoretical) {
					std::cout << "THEORETICAL:\n";
					runTheoretical(filename, k, epsilon);
				}

				std::cout << std::endl;

				if (run_practical) {
					std::cout << "PRACTICAL:\n";
					runPractical(filename, k, epsilon);
					// runPractical(filename, k, epsilon, UpdateAlgorithm::NaiveSingleDist);
					// runPractical(filename, k, epsilon, UpdateAlgorithm::Randomized);
				}

				std::cout << std::endl;
				std::cout << std::endl;
			}
		}
	}
}

void analyzeGraphs()
{
	auto print_info = [](std::string const& filename) {
		IncrementalGraph graph(filename);

		std::cout << "Graph: " << filename << "\n";
		std::cout << "Number of nodes: " << graph.numberOfNodes() << "\n";
		std::cout << "Number of edges: " << graph.numberOfEdges() << "\n";
		std::cout << "Number of future edges: " << graph.numberOfFutureEdges() << "\n";

		while (graph.hasNextEdge()) { graph.addNextEdge(); }
		GraphAlgs graph_algs(graph);

		std::cout << "Number of components: " << graph_algs.numberOfComponents() << "\n";
		std::cout << "Size of largest component: " << graph_algs.sizeOfLargestComponent() << "\n";

		std::cout << std::endl;
	};

	for (auto const& filename: FILENAMES) { print_info(filename); }
	for (auto const& filename: FILENAMES_BESTFRAC) { print_info(filename); }
	for (auto const& filename: FILENAMES_COLLABORATION) { print_info(filename); }
}

void implicitBucketIncreaseCount()
{
	// std::vector<int> const ks = {5, 10, 20, 50, 100, 250, 500};
	std::vector<int> const ks = {10, 50, 200};
	// auto const epsilons = {2., 1., .5, .2, .1};
	auto const epsilons = {2., .5, .1};
	auto const alg = UpdateAlgorithm::StarNodeSingleDist;

	for (auto const& filename: FILENAMES) {
		IncrementalGraph graph(filename);
		if ((int)graph.numberOfNodes() < ks.back()) { continue; }

		std::cout << "graph: " << filename << std::endl;
		for (auto k: ks) {
			std::cout << "k: " << k << std::endl;
			for (auto epsilon: epsilons) {
				// std::cout << "epsilon: " << epsilon << std::endl;
				IncrementalGraph graph(filename);
				auto args = IncrementalKCenter::Arguments{alg, k, (double)50, epsilon};
				IncrementalKCenter ikcenter(graph, args);
				run(graph, ikcenter);

				std::cout << ikcenter.getImplicitBucketIncreaseCount() << " ";
			}
			std::cout << std::endl;
		}
	}
}

// This experiment does too much other stuff, but this was just the quickest way
// to get this information...
void maxNumberOfActiveComponents()
{
	for (auto const& filename: FILENAMES) {
		std::cout << "graph: " << filename << std::endl;
		IncrementalGraph graph(filename);
		auto args = IncrementalKCenter::Arguments{UpdateAlgorithm::StarNodeSingleDist, 10, (double)50, 1.};
		IncrementalKCenter ikcenter(graph, args);

		std::size_t max = 0;
		std::size_t active_edge_insert_count = 0;
		while (graph.hasNextEdge()) {
			auto new_edge = graph.addNextEdge();
			ikcenter.update(new_edge);
			max = std::max(max, ikcenter.getNumberOfActiveComponents());
			if (ikcenter.isActive(new_edge.id1) && ikcenter.isActive(new_edge.id2)) {
				++active_edge_insert_count;
			}
		}


		std::cout << "max #active components: " << max << std::endl;
		auto ratio = (double)active_edge_insert_count/graph.numberOfEdges();
		std::cout << "ratio of active edge inserts: " << ratio << std::endl;
	}
}

void runAll(std::vector<std::string> const& filenames, std::vector<int> const& ks,
                  std::vector<double> const& epsilons, UpdateAlgorithm alg)
{
	for (auto const& filename: filenames) {
		for (auto k: ks) {
			for (double epsilon: epsilons) {
				std::cout << "\n\n";
				std::cout << "Running experiment with:\n";
				std::cout << "graph: " << filename << "\n";
				std::cout << "k: " << k << "\n";
				std::cout << "epsilon: " << epsilon << "\n";
				std::cout << std::endl;

				runPractical(filename, k, epsilon, alg);
			}
		}
	}
}

int main(int argc, char* argv[])
{
	if (argc > 2) {
		printUsage();
		Error("Wrong number of parameters provided.");
	}

	std::string exp;
	if (argc == 2) {
		exp = argv[1];
	}


	auto print_exp_header = [](std::string const& name) {
		std::cout << std::endl;
		std::cout << "EXPERIMENT: " << name << "\n";
		std::cout << "==========================\n";
	};

	if (exp == "" || exp == "1") {
		print_exp_header("1");
		experiment1();
	}
	if (exp == "" || exp == "2") {
		print_exp_header("2");
		compare_theory_and_pratical(false, true);
	}
	if (exp == "" || exp == "3") {
		print_exp_header("3");
		analyzeGraphs();
	}
	if (exp == "" || exp == "4") {
		print_exp_header("4");
		implicitBucketIncreaseCount();
	}
	if (exp == "" || exp == "5") {
		print_exp_header("5");
		maxNumberOfActiveComponents();
	}
	if (exp == "" || exp == "6") {
		print_exp_header("6");

		runAll(
			FILENAMES,
			{5, 10, 20, 50, 100, 250, 500},
			{2., .5, .1},
			UpdateAlgorithm::StarNodeSingleDist
		);
	}
	if (exp == "" || exp == "7") {
		print_exp_header("7");

		auto const alg = UpdateAlgorithm::StarNodeSingleDist;
		for (auto const& filename: FILENAMES) {
			for (auto k: {50, 100, 250, 500}) {
				for (double epsilon: {2., .5, .1}) {
					std::cout << "\n\n";
					std::cout << "Running experiment with:\n";
					std::cout << "graph: " << filename << "\n";
					std::cout << "k: " << k << "\n";
					std::cout << "epsilon: " << epsilon << "\n";
					std::cout << std::endl;

					std::cout << "NAIVE:" << std::endl;
					runPractical(filename, k, epsilon, alg, false);
					std::cout << "\nNON-NAIVE:" << std::endl;
					runPractical(filename, k, epsilon, alg, true);
				}
			}
		}
	}
	if (exp == "" || exp == "8") {
		bool star = true;
		bool star_naive = true;
		bool random = false;
		bool naive = false;

		// auto const ks = {10, 20, 50, 100, 200, 500};
		auto const ks = {9, 19, 49, 99, 199, 499}; // in the paper, k is the dispersion and not clustering value
		auto const epsilon = .1;
		auto const& filenames = FILENAMES;

		for (auto k: ks) {
			std::vector<PracticalResult> star_results;
			std::vector<PracticalResult> star_naive_results;
			std::vector<PracticalResult> randomized_results;
			std::vector<PracticalResult> naive_results;
			PracticalResult star_result;
			PracticalResult star_naive_result;
			PracticalResult randomized_result;
			PracticalResult naive_result;

			for (auto const& filename: filenames) {
				std::cout << "Running experiment with:\n";
				std::cout << "graph: " << filename << "\n";
				std::cout << std::endl;

				if (star) {
					std::cout << "Star Node:" << std::endl;
					std::cout << "==========" << std::endl;
					star_result = runPractical(filename, k, epsilon);
					std::cout << std::endl;
				}

				if (star_naive) {
					std::cout << "Star Node with Naive Gonzalez:" << std::endl;
					std::cout << "==============================" << std::endl;
					star_naive_result = runPractical(filename, k, epsilon, UpdateAlgorithm::StarNodeSingleDist, false);
					std::cout << std::endl;
				}

				if (random) {
					std::cout << "Randomized:" << std::endl;
					std::cout << "===========" << std::endl;
					randomized_result = runPractical(filename, k, epsilon, UpdateAlgorithm::Randomized);
					std::cout << std::endl;
				}

				if (naive) {
					std::cout << "Naive:" << std::endl;
					std::cout << "======" << std::endl;
					naive_result = runPractical(filename, k, epsilon, UpdateAlgorithm::NaiveSingleDist);
					std::cout << std::endl;
				}

				std::cout << std::endl;

				star_results.push_back(star_result);
				star_naive_results.push_back(star_naive_result);
				randomized_results.push_back(randomized_result);
				naive_results.push_back(naive_result);
			}

			std::cout << "[" << std::endl;
			for (std::size_t i = 0; i < filenames.size(); ++i) {
				std::cout << "( " << star_results[i].far_node_update_count_precise << ", " << star_naive_results[i].far_node_update_count_precise << ", " << randomized_results[i].far_node_update_count_precise << ", " << naive_results[i].far_node_update_count_precise << " )," << std::endl;
			}
			std::cout << "]" << std::endl;
		}
	}
}
