
#include "defs.h"
#include "incremental_graph.h"
#include "incremental_k_center.h"
#include "incremental_k_center_randomized.h"
#include "measurement_tool.h"
#include "random.h"

#include <iostream>
#include <string>

void printUsage()
{
	std::cout << "Usage: ./main <graph_file> <k> <epsilon> [<algorithm>]\n";
	std::cout << "The algorithm strings can be seen in IncrementalKCenter::toUpdateAlgorithm.\n";
}

void run(IncrementalGraph& graph, IncrementalKCenterRandomized& ikcenter)
{
	std::cout << "Starting to run the incremental k-center theory algorithm." << std::endl;
	std::size_t round_update_count = 0;
	while (graph.hasNextEdge()) {
		MEASUREMENT::start(EXP::AddEdge);
		auto new_edge = graph.addNextEdge();
		MEASUREMENT::stop(EXP::AddEdge);

		MEASUREMENT::start(EXP::KCenterUpdate);
		ikcenter.update(new_edge);
		MEASUREMENT::stop(EXP::KCenterUpdate);

		if (ikcenter.farNodesChangedLastUpdate()) {
			++round_update_count;
		}
	}

	std::cout << "Number of rounds in which the far nodes changed: " << round_update_count << std::endl;
	// std::cout << "Number of far nodes changed: " << ikcenter.getFarNodeUpdateCount() << std::endl;

	MEASUREMENT::print();
}

void run(IncrementalGraph& graph, IncrementalKCenter& ikcenter)
{
	std::cout << "Starting to run the incremental k-center algorithm." << std::endl;
	std::size_t round_update_count = 0;
	while (graph.hasNextEdge()) {
		MEASUREMENT::start(EXP::AddEdge);
		auto new_edge = graph.addNextEdge();
		MEASUREMENT::stop(EXP::AddEdge);

		MEASUREMENT::start(EXP::KCenterUpdate);
		ikcenter.update(new_edge);
		MEASUREMENT::stop(EXP::KCenterUpdate);

		if (ikcenter.farNodesChangedLastUpdate()) {
			// std::cout << "Distance after update: "
			//           << ikcenter.currentKCenterDistance() << std::endl;
			++round_update_count;
		}
	}

	std::cout << "Number of rounds in which the far nodes changed: " << round_update_count << std::endl;
	std::cout << "Number of far nodes changed: " << ikcenter.getFarNodeUpdateCount() << std::endl;

	MEASUREMENT::print();
}

void runAdversarial(IncrementalGraph& graph, IncrementalKCenter& ikcenter)
{
	Random random;

	std::cout << "Starting to run the adversarial incremental k-center algorithm." << std::endl;
	std::size_t round_update_count = 0;
	while (ikcenter.currentKCenterDistance() > 5) {
		MEASUREMENT::start(EXP::AddEdge);
		auto const& far_nodes = ikcenter.getCurrentFarNodes();
		std::size_t index1 = 0, index2 = 0;
		while (index1 == index2) {
			index1 = random.getSizeT(0, far_nodes.size()-1);
			index2 = random.getSizeT(0, far_nodes.size()-1);
		}
		Edge new_edge{far_nodes[index1], far_nodes[index2], 0};
		bool was_added = graph.addUnplannedEdge(new_edge);
		assert(was_added);
		MEASUREMENT::stop(EXP::AddEdge);

		MEASUREMENT::start(EXP::KCenterUpdate);
		ikcenter.update(new_edge);
		MEASUREMENT::stop(EXP::KCenterUpdate);

		if (ikcenter.farNodesChangedLastUpdate()) {
			// std::cout << "Distance after update: "
			//           << ikcenter.currentKCenterDistance() << std::endl;
			++round_update_count;
		}
	}

	std::cout << "Number of rounds in which the far nodes changed: " << round_update_count << std::endl;
	std::cout << "Number of far nodes changed: " << ikcenter.getFarNodeUpdateCount() << std::endl;

	MEASUREMENT::print();
}

int main(int argc, char* argv[])
{
	// Parsing the input parameters
	if (argc < 4 || argc > 5) {
		printUsage();
		Error("Wrong number of parameters provided.");
	}
	std::string const filename = argv[1];
	int const k = std::stoi(argv[2]);
	double const epsilon = std::stod(argv[3]);
	IncrementalKCenter::UpdateAlgorithm update_algorithm;
	bool theory_algorithm = false;

	std::cout << "k = " << k << std::endl;
	std::cout << "epsilon = " << epsilon << std::endl;

	update_algorithm = IncrementalKCenter::UpdateAlgorithm::StarNodeSingleDist;
	if (argc > 4) {
		if (std::string(argv[4]) == "theory") {
			theory_algorithm = true;
		}
		else {
			update_algorithm = IncrementalKCenter::toUpdateAlgorithm(argv[4]);
		}
		std::cout << "algorithm: " << argv[4] << std::endl;
	}
	else {
		std::cout << "algorithm: default" << std::endl;
	}

	if (k <= 0) {
		Error("The parameter k has to be positive.");
	}

	// Reading the graph and initializing the incremental k-center clustering algorithm
	std::cout << "Reading and processing input graph: " << filename << std::endl;
	MEASUREMENT::start(EXP::ReadGraph);
	IncrementalGraph graph(filename);
	MEASUREMENT::stop(EXP::ReadGraph);

	if (theory_algorithm) {
		std::cout << "Initializing the incremental k-center algorithm." << std::endl;
		MEASUREMENT::start(EXP::InitKCenter);
		IncrementalKCenterRandomized ikcenter(graph, k, epsilon);
		MEASUREMENT::stop(EXP::InitKCenter);

		run(graph, ikcenter);
	}
	else {
		std::cout << "Initializing the incremental k-center algorithm." << std::endl;
		MEASUREMENT::start(EXP::InitKCenter);
		auto args = IncrementalKCenter::Arguments{update_algorithm, k, (double)k, epsilon};
		IncrementalKCenter ikcenter(graph, args);
		MEASUREMENT::stop(EXP::InitKCenter);

		run(graph, ikcenter);
		// runAdversarial(graph, ikcenter);
	}
}
