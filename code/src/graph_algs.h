#pragma once

#include "basic_types.h"
#include "incremental_graph.h"
#include "resettable_vector.h"
#include "union_find.h"

#include <queue>

class GraphAlgs
{
public:
	GraphAlgs(IncrementalGraph const& graph);

	// This BFS sets the distance of start_id to 0, all others to infinity, and then
	// starts a BFS from start_id.
	void freshBFS(NodeID start_id, Distances& distances);

	// This BFS just updates nodes that are affected by a recent distance change of start_id.
	void updateBFS(NodeID start_id, Distances& distances);
	// A version of the above function where f is executed on each node_id that becomes
	// closest to start_id.
	template <typename F>
	void updateBFS(NodeID start_id, Distances& distances, F const& f, Distance radius = c::dist_max);

	// A custom version for the randomized algorithm
	template <typename F>
	void updateBFSWithFarNodeCount(NodeID start_id, Distances& distances, F const& f,
	                               Distance current_distance, std::size_t& number_of_far_nodes);

	// These functions are made for short searches and thus the internal data structures are
	// manually reset. If you want to use this for long searches, add another version with
	// a hard reset.
	Distance smallestDistance(NodeID source, NodeIDs const& destinations);
	Distance largestDistance(NodeID source, NodeID ignore);

	std::size_t numberOfComponents();
	std::size_t sizeOfLargestComponent();

private:
	IncrementalGraph const& graph;
	std::vector<bool> is_destination;
	ResettableVector<Distance> distances;

	void BFS(NodeID start_id, Distances& distances, bool reset);
	void setDestinations(NodeIDs const& destinations, bool value);

	UnionFind<NodeID> buildUnionFind();
};

template <typename F>
void GraphAlgs::updateBFS(NodeID start_id, Distances& distances, F const& f, Distance radius)
{
	std::queue<NodeID> Q;

	distances[start_id] = 0;
	f(start_id);
	Q.push(start_id);

	while (!Q.empty()) {
		auto current_node_id = Q.front();
		Q.pop();

		if (distances[current_node_id] >= radius) {
			continue;
		}

		for (auto neighbor_id: graph.getNeighborsOf(current_node_id)) {
			if (distances[neighbor_id] > distances[current_node_id] + 1) {
				distances[neighbor_id] = distances[current_node_id] + 1;
				f(neighbor_id);
				Q.push(neighbor_id);
			}
		}
	}
}

template <typename F>
void GraphAlgs::updateBFSWithFarNodeCount(NodeID start_id, Distances& distances, F const& f,
                                          Distance current_distance, std::size_t& number_of_far_nodes)
{
	std::queue<NodeID> Q;

	if (distances[start_id] >= current_distance) { --number_of_far_nodes; }
	distances[start_id] = 0;
	f(start_id);
	Q.push(start_id);

	while (!Q.empty()) {
		auto current_node_id = Q.front();
		Q.pop();

		for (auto neighbor_id: graph.getNeighborsOf(current_node_id)) {
			if (distances[neighbor_id] > distances[current_node_id] + 1) {
				if (distances[neighbor_id] >= current_distance &&
				    distances[current_node_id] + 1 < current_distance) {
					--number_of_far_nodes;
				}

				distances[neighbor_id] = distances[current_node_id] + 1;
				f(neighbor_id);
				Q.push(neighbor_id);
			}
		}
	}
}
