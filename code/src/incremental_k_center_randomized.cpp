#include "incremental_k_center_randomized.h"
#include "basic_types.h"

IncrementalKCenterRandomized::IncrementalKCenterRandomized(
	IncrementalGraph const& graph, int const k, double const epsilon)
	: graph(graph), k(k), q(k), epsilon(epsilon), component_threshold(graph.numberOfNodes()/q)
	, graph_algs(graph), is_active(graph.numberOfNodes(), false), number_of_active_components(0)
	, in_boundary(graph.numberOfNodes(), false)
{
	init();
}

void IncrementalKCenterRandomized::init()
{
	min_distance = 1;
	if (graph_algs.numberOfComponents() == 1) {
		max_distance = 2*graph_algs.largestDistance(0, NodeID());
	}
	else {
		max_distance = graph.numberOfNodes();
	}

	initComponents();

	std::size_t bucket_index = 0;
	for (unsigned int i = 0; std::ceil(pow(1+epsilon, i)*min_distance) <= (1+epsilon)*max_distance; ++i) {
		Distance const new_distance = std::ceil(pow(1+epsilon, i)*min_distance);
		if (buckets.empty() || new_distance != buckets.back().distance) {
			buckets.push_back(Bucket{
				bucket_index,
				new_distance,
				NodeIDs(),
				Distances(graph.numberOfNodes(), c::dist_max),
				std::vector<std::size_t>(graph.numberOfNodes(), c::invalid_index),
				0
			});
			++bucket_index;

			if (hasValidClustering()) {
				initialGonzalez(buckets.back());
			}
		}
	}
}

void IncrementalKCenterRandomized::initComponents()
{
	union_find.init(graph.numberOfNodes());

	for (auto node_id: graph.getNodeIDsRange()) {
		for (auto neighbor_id: graph.getNeighborsOf(node_id)) {
			union_find.addRelation({node_id, neighbor_id});
		}
	}

	for (auto node_id: graph.getNodeIDsRange()) {
		if (!is_active[node_id] && union_find.getComponentSize(node_id) >= component_threshold) {
			addComponentToActiveNodes(node_id);
			++number_of_active_components;
		}
	}
}

void IncrementalKCenterRandomized::initialGonzalez(Bucket& bucket)
{
	auto& centers = bucket.centers;
	auto& distances = bucket.distances;
	auto& closest_center = bucket.closest_center;
	auto& number_of_far_nodes = bucket.number_of_far_nodes;
	auto const distance = bucket.distance;

	centers.clear();
	std::fill(distances.begin(), distances.end(), c::dist_max);
	std::fill(closest_center.begin(), closest_center.end(), c::invalid_index);
	number_of_far_nodes = active_nodes.size();

	auto set_close = [&](NodeID node_id) {
		// only set closest_center if there is a close center!
		if (closest_center[node_id] == c::invalid_index && distances[node_id] <= distance) {
			--number_of_far_nodes;
		}
		closest_center[node_id] = centers.size()-1;
	};

	// choose initial far node and make BFS
	centers.push_back(active_nodes.front());
	// ++center_updates;
	graph_algs.updateBFS(centers.back(), distances, set_close, distance);

	incrementalGonzalez(bucket, false);
}

void IncrementalKCenterRandomized::incrementalGonzalez(Bucket& bucket, bool count_updates)
{
	auto& centers = bucket.centers;
	auto& distances = bucket.distances;
	auto& closest_center = bucket.closest_center;
	auto& number_of_far_nodes = bucket.number_of_far_nodes;
	auto const distance = bucket.distance;

	auto set_close = [&](NodeID node_id) {
		// only set closest_center if there is a close center!
		if (closest_center[node_id] == c::invalid_index && distances[node_id] <= distance) {
			--number_of_far_nodes;
		}
		closest_center[node_id] = centers.size()-1;
	};

	while (number_of_far_nodes > 0 && (int)centers.size() != k+1) {
		// choose a node which is far from all centers uniformly at random
		std::size_t random_count = random.getSizeT(1, number_of_far_nodes);
		std::size_t counter = 0;
		NodeID new_center_id;
		for (NodeID node_id: active_nodes) {
			if (distances[node_id] > distance) {
				++counter;
				if (counter == random_count) {
					new_center_id = node_id;
					break;
				}
			}
		}

		assert(new_center_id.valid());

		// Make max_node a far node
		centers.push_back(new_center_id);
		if (count_updates) { ++center_updates; }
		graph_algs.updateBFS(centers.back(), distances, set_close, distance);
	}
}

void IncrementalKCenterRandomized::update(Edge const& edge)
{
	far_nodes_changed_last_update = false;

	auto const had_valid_clustering = hasValidClustering();
	updateComponents(edge.id1, edge.id2);

	// If none of the endpoints belongs to an active component, this edge insertion
	// does not influence the current clustering.
	if (!is_active[edge.id1] && !is_active[edge.id2]) { return; }

	if (hasValidClustering()) {
		if (had_valid_clustering) {
			for (auto& bucket: buckets) {
				if (isClustering(bucket)) {
					if (introducedFarNode(bucket, edge)) {
						initialGonzalez(bucket);
					}
					else {
						// We still have to correctly maintain the distances in case
						// a component merge happens and we have to check if we obtain
						// a dispersion.
						updateDistances(bucket, edge);
					}
				}
				else {
					std::size_t delete_index = updateDistances(bucket, edge);
					if (delete_index != c::invalid_index) {
						far_nodes_changed_last_update = true;
						updateFarNodes(bucket, delete_index);
					}
				}
			}
		}
		else {
			for (auto& bucket: buckets) {
				initialGonzalez(bucket);
			}
		}
	}
}

bool IncrementalKCenterRandomized::introducedFarNode(Bucket& bucket, Edge const& edge)
{
	auto& distances = bucket.distances;
	auto const distance = bucket.distance;

	Distance dist1 = distances[edge.id1];
	Distance dist2 = distances[edge.id2];

	// This can only be the case if two small components were merged and then formed a large one.
	if (dist1 == c::dist_max && dist2 == c::dist_max) {
		assert(is_active[edge.id1] && is_active[edge.id2]);
		return true;
	}

	NodeID active_id, inactive_id;
	Distance center_distance;
	if (dist1 == c::dist_max) {
		active_id = edge.id2;
		inactive_id = edge.id1;
		center_distance = dist2;
	}
	else if (dist2 == c::dist_max) {
		active_id = edge.id1;
		inactive_id = edge.id2;
		center_distance = dist1;
	}
	else {
		return false;
	}

	assert(center_distance != c::dist_max);
	auto component_diameter = graph_algs.largestDistance(inactive_id, active_id);

	return (center_distance + component_diameter + 1 > (1.+epsilon)*distance);
}

bool IncrementalKCenterRandomized::farNodesChangedLastUpdate() const
{
	return far_nodes_changed_last_update;
}

NodeIDs IncrementalKCenterRandomized::getCurrentCenters() const
{
	for (auto const& bucket: buckets) {
		if (isClustering(bucket)) {
			return bucket.centers;
		}
	}

	assert(false);
}

NodeIDs IncrementalKCenterRandomized::getCurrentFarNodes() const
{
	for (std::size_t i = 0; i < buckets.size(); ++i) {
		if (isClustering(buckets[i])) {
			if (i == 0) { return buckets[i].centers; }
			else { return buckets[i-1].centers; }
		}
	}

	assert(false);
}

Distance IncrementalKCenterRandomized::currentKCenterDistance() const
{
	for (std::size_t i = 0; i < buckets.size(); ++i) {
		if (isClustering(buckets[i])) {
			if (i == 0) { return buckets[i].distance; }
			else { return buckets[i-1].distance; }
		}
	}

	assert(false);
}

NodeIDs const& IncrementalKCenterRandomized::getActiveNodes() const
{
	return active_nodes;
}

bool IncrementalKCenterRandomized::isActive(NodeID node_id) const
{
	return is_active[node_id];
}

std::size_t IncrementalKCenterRandomized::getFarNodeUpdateCount() const
{
	return center_updates;
}

bool IncrementalKCenterRandomized::hasValidClustering() const
{
	return number_of_active_components > 0 && (int)number_of_active_components <= k;
}

// returns c::invalid_index if no nodes got close
std::size_t IncrementalKCenterRandomized::updateDistances(Bucket& bucket, Edge const& edge)
{
	auto& distances = bucket.distances;
	auto& closest_center = bucket.closest_center;
	auto const distance = bucket.distance;

	if ((closest_center[edge.id1] == c::invalid_index && closest_center[edge.id2] == c::invalid_index) ||
		std::min(distances[edge.id1], distances[edge.id2]) >= distance) {
		return c::invalid_index;
	}

	bool far_nodes_got_close =
		(closest_center[edge.id1] != c::invalid_index && closest_center[edge.id2] != c::invalid_index &&
		closest_center[edge.id1] != closest_center[edge.id2] &&
		distances[edge.id1] + distances[edge.id2] + 1 < distance);

	// distances are int, no over-/underflow (even if one of them is c::dist_max)
	Distance diff = distances[edge.id1] - distances[edge.id2];

	std::vector<bool> got_close(k+1, false);
	auto calc_delete_index = [&]() {
		bool saw = false;
		for (std::size_t i = 0; i < got_close.size(); ++i) {
			if (got_close[i]) {
				if (saw) { return i; }
				saw = true;
			}
		}

		assert(!saw);
		return c::invalid_index;
	};

	if (far_nodes_got_close) {
		got_close[closest_center[edge.id1]] = true;
		got_close[closest_center[edge.id2]] = true;
	}

	if (std::abs(diff) <= 1) {
		// No distance can changes in this case! But maybe two far nodes got close.
		// If this is the case, this is the only node pair that got close.
		return calc_delete_index();
	}

	NodeID start_id;
	if (diff > 1) {
		// The first node has a larger distance, thus its distance changes by
		// the path from the center to the second node and over the new edge.
		distances[edge.id1] = distances[edge.id2] + 1;
		closest_center[edge.id1] = closest_center[edge.id2];
		start_id = edge.id1;
	}
	else { // diff < -1
		// Analogous to the previous case, except now the distance of the second
		// node is larger.
		distances[edge.id2] = distances[edge.id1] + 1;
		closest_center[edge.id2] = closest_center[edge.id1];
		start_id = edge.id2;
	}

	assert(closest_center[start_id] != c::invalid_index);

	std::queue<NodeID> Q;
	Q.push(start_id);

	while (!Q.empty()) {
		auto current_node_id = Q.front();
		Q.pop();

		if (distances[current_node_id] >= distance) {
			continue;
		}

		for (auto neighbor_id: graph.getNeighborsOf(current_node_id)) {
			// first check if two far nodes got close
			if (closest_center[current_node_id] != closest_center[neighbor_id] &&
				distances[current_node_id] + distances[neighbor_id] + 1 < distance) {

				// TODO: the first line should not be necessary -- try to remove at some point
				if (closest_center[neighbor_id] != c::invalid_index) {
					got_close[closest_center[current_node_id]] = true;
					got_close[closest_center[neighbor_id]] = true;
				}
			}

			// now do the normal BFS exploration
			if (distances[neighbor_id] > distances[current_node_id] + 1) {
				distances[neighbor_id] = distances[current_node_id] + 1;
				closest_center[neighbor_id] = closest_center[current_node_id];
				Q.push(neighbor_id);
			}
		}
	}

	return calc_delete_index();
}

void IncrementalKCenterRandomized::updateFarNodes(Bucket& bucket, std::size_t delete_index)
{
	assert(delete_index != c::invalid_index);

	auto& centers = bucket.centers;
	auto& distances = bucket.distances;
	auto& closest_center = bucket.closest_center;
	auto& number_of_far_nodes = bucket.number_of_far_nodes;
	auto const distance = bucket.distance;

	// delete all centers starting from delete_index
	centers.resize(delete_index);

	number_of_far_nodes = 0;

	//
	// find boundary
	//

	boundary.clear();
	in_boundary.reset();
	for (NodeID node_id: active_nodes) {
		// ignore nodes close to the surviving centers
		if (closest_center[node_id] < delete_index) { continue; }

		++number_of_far_nodes;
		closest_center[node_id] = c::invalid_index;
		distances[node_id] = c::dist_max;

		for (auto neighbor_id: graph.getNeighborsOf(node_id)) {
			if (!in_boundary[neighbor_id] && closest_center[neighbor_id] < delete_index) {
				in_boundary.set(neighbor_id, true);
				boundary.push_back(neighbor_id);
			}
		}
	}
	assert(!boundary.empty());

	//
	// expand boundary
	//

	// sort boundary in decreasing distance
	auto dec_far_node_dist = [&](NodeID node_id1, NodeID node_id2) {
		return distances[node_id1] > distances[node_id2];
	};
	std::sort(boundary.begin(), boundary.end(), dec_far_node_dist);

	std::queue<NodeID> todo;
	todo.push(boundary.back());

	// update from boundary to inside
	while (!todo.empty()) {
		auto node_id = todo.front();
		todo.pop();

		// before expanding, insert all the boundary nodes of this distance
		while (!boundary.empty() && distances[boundary.back()] == distances[node_id]) {
			todo.push(boundary.back());
			boundary.pop_back();
		}
		
		// don't expland nodes at maximal distance
		if (distances[node_id] >= distance) { continue; }

		// update the neighbors
		for (auto neighbor_id: graph.getNeighborsOf(node_id)) {
			if (closest_center[neighbor_id] == c::invalid_index) {
				closest_center[neighbor_id] = closest_center[node_id];
				distances[neighbor_id] = distances[node_id] + 1;
				--number_of_far_nodes;
				todo.push(neighbor_id);
			}
		}
	}

	//
	// run "randomized Gonzalez" again
	//

	incrementalGonzalez(bucket);
}

void IncrementalKCenterRandomized::updateComponents(NodeID node_id1, NodeID node_id2)
{
	if (union_find.belongToSameComponent(node_id1, node_id2)) {
		return;
	}

	auto size1 = union_find.getComponentSize(node_id1);
	auto size2 = union_find.getComponentSize(node_id2);

	union_find.addRelation({node_id1, node_id2});

	// two large components are merged
	if (size1 >= component_threshold && size2 >= component_threshold) {
		assert(is_active[node_id1] && is_active[node_id2]);
		--number_of_active_components;
	}
	// the small second component is merged into the large first
	else if (size1 >= component_threshold) {
		assert(is_active[node_id1] && !is_active[node_id2]);
		addComponentToActiveNodes(node_id2);
	}
	// the small first component is merged into the large second
	else if (size2 >= component_threshold) {
		assert(!is_active[node_id1] && is_active[node_id2]);
		addComponentToActiveNodes(node_id1);
	}
	// two small components are merged
	else if (size1 + size2 >= component_threshold) {
		assert(!is_active[node_id1] && !is_active[node_id2]);
		addComponentToActiveNodes(node_id1);
		++number_of_active_components;
	}
}

void IncrementalKCenterRandomized::addComponentToActiveNodes(NodeID start_id)
{
	NodeIDs stack;

	stack.push_back(start_id);
	active_nodes.push_back(start_id);
	is_active[start_id] = true;

	while (!stack.empty()) {
		auto current_id = stack.back();
		stack.pop_back();

		for (auto neighbor_id: graph.getNeighborsOf(current_id)) {
			if (!is_active[neighbor_id]) {
				stack.push_back(neighbor_id);
				active_nodes.push_back(neighbor_id);
				is_active[neighbor_id] = true;
			}
		}
	}
}

void IncrementalKCenterRandomized::checkDistances(std::string const& s)
{
	for (auto const& bucket: buckets) {
		checkDistances(bucket);
	}
}

void IncrementalKCenterRandomized::checkDistances(Bucket const& bucket, std::string const& s)
{
	Distances dists(graph.numberOfNodes(), c::dist_max);
	dists[bucket.centers.front()] = 0;
	graph_algs.freshBFS(bucket.centers.front(), dists);
	for (std::size_t i = 1; i < bucket.centers.size(); ++i) {
		dists[bucket.centers[i]] = 0;
		graph_algs.updateBFS(bucket.centers[i], dists);
	}

	for (std::size_t i = 0; i < dists.size(); ++i) {
		if (bucket.distances[i] <= bucket.distance) {
			assert(bucket.distances[i] == dists[i]);
		}
		else {
			assert(dists[i] > bucket.distance);
		}
	}
}
