#pragma once

#include "basic_types.h"
#include "graph_algs.h"
#include "incremental_graph.h"
#include "union_find.h"
#include "resettable_vector.h"

namespace unit_tests { void testIncrementalKCenter(); }

// this type is supposed to store at each ID the ID of its center
using Clustering = std::vector<NodeID>;

class IncrementalKCenter
{
public:
	//
	// UpdateAlgorithm
	//
	enum class UpdateAlgorithm {
		TotallyNaive,
		Naive,
		NaiveSingleDist,
		Postfix,
		StarNode,
		StarNodeSingleDist,
		Randomized
	};
	static UpdateAlgorithm toUpdateAlgorithm(std::string const& update_algorithm_string);
	static std::string toString(UpdateAlgorithm update_algorithm);
	static std::vector<UpdateAlgorithm> getAllUpdateAlgorithms();

	struct Arguments {
		UpdateAlgorithm alg;
		int const k;
		double const q;
		double const epsilon;
	};

	//
	// public member functions
	//

	IncrementalKCenter(IncrementalGraph const& graph, Arguments const& args);

	void update(Edge const& edge);
	bool centersChangedLastUpdate() const;
	bool farNodesChangedLastUpdate() const;
	bool distanceChangedLastUpdate() const;

	bool hasValidClustering() const;
	NodeIDs const& getCurrentCenters() const;
	NodeIDs const& getCurrentFarNodes() const;
	Distance currentKCenterDistance() const;
	NodeIDs const& getActiveNodes() const;
	bool isActive(NodeID node_id) const;
	std::size_t getNumberOfActiveComponents() const;

	Clustering computeClusteringVector();

	std::size_t getFarNodeUpdateCount() const;
	std::size_t getDecFarNodeUpdateCount() const;
	std::size_t getIncFarNodeUpdateCount() const;
	std::size_t getImplicitBucketIncreaseCount() const;

	void useNonNaiveGonzalez(bool use_non_naiv_gonzalez = true);

private:
	IncrementalGraph const& graph;
	GraphAlgs graph_algs;
	Random random;
	int const k;
	double const q;
	double const epsilon;
	std::size_t const component_threshold;
	UpdateAlgorithm const update_algorithm;
	bool use_non_naiv_gonzalez = true;

	Distance current_distance;
	NodeIDs far_nodes;
	NodeIDs centers;
	bool centers_changed_last_update;
	bool far_nodes_changed_last_update;
	bool distance_changed_last_update;
	std::size_t far_node_updates = 0;
	std::size_t dec_far_node_updates = 0;
	std::size_t inc_far_node_updates = 0;
	std::size_t implicit_bucket_increase_count = 0;

	// component related members
	UnionFind<NodeID> union_find;
	// This contains all the nodes that are in a large component, i.e., a component
	// that has size at least "component_threshold".
	NodeIDs active_nodes;
	std::vector<bool> is_active;
	std::size_t number_of_active_components;

	// Data for Naive, Postfix, and StarNode
	std::vector<Distances> far_nodes_distances_vec;
	std::vector<Distances> centers_distances_vec;

	// Data for StarNodeSingleDist and part for NaiveSingleDist, and Randomized
	Distances far_node_dist;
	Distances center_dist;
	std::vector<std::size_t> closest_far_nodes;
	std::size_t star_node_index;
	NodeIDs boundary;
	ResettableVector<bool> in_boundary;

	// Data for Randomized
	std::size_t number_of_far_nodes;

	void init();
	void initComponents();
	void initialGonzalez(bool count_updates = true);
	void updateGonzalez();
	void updateGonzalezSingleDist();

	void updateComponents(NodeID node_id1, NodeID node_id2);
	void addComponentToActiveNodes(NodeID start_id);

	bool introducedFarNode(Edge const& edge);
	bool bothEndpointsFar(Edge const& edge);

	void updateDistancesTotallyNaive();
	void updateDistances(Edge const& edge);
	void updateFarNodesDistances(Edge const& edge);
	void updateClosestFarNodeDistances(Edge const& edge);

	void updateFarNodes();
	void updateFarNodesNaive();
	void updateFarNodesNaiveSingleDist();
	void updateFarNodesPostfix();
	void updateFarNodesStarNode();
	void updateFarNodesStarNodeSingleDist();
	void updateFarNodesRandomized();
	Distance updateFarNodesSingleDist(std::size_t far_node_index);

	void runGonzalez(bool count_updates = true);
	void runGonzalezSingleDist(bool count_updates = true);
	void runGonzalezFrom(std::size_t first_index);
	void runGonzalezRandomized(bool count_updates = true);
	void runGonzalezRandomizedInnerLoop(bool count_updates = true);
	void decreaseCurrentDistance();
	void computeDistancesFrom(NodeID node_id, Distances& distances);
	void updateDistancesFrom(NodeID node_id, Distances& distances);

	// small helpers
	void updateMinDistances(Distances& distances, Distances& min_distances);
	void updateMinDistancesSingle(Distances& distances, std::size_t far_node_index);
};
