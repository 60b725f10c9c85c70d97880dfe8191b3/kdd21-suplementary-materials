def read_number_of_updates(filename):
    num_updates = []
    with open(filename) as file:
        for line in file:
            if "future" in line:
                num_updates.append(int(line.split()[-1]))

    num_updates = num_updates[len(num_updates)//2:len(num_updates)]
    return num_updates

def read_total_times(filename):
    total_times = []
    with open(filename) as file:
        for line in file:
            if "TOTAL" in line:
                total_times.append(float(line.split()[-2]))

    total_times = [ total_times[2*i] for i in range(len(total_times)//2) ]
    return total_times

def print_times_per_update(num_updates, total_times):
    ks = [10, 20, 50, 100, 200, 500]

    assert(len(ks)*len(num_updates) == len(total_times))

    time_per_update = [ total_times[i]/num_updates[i%len(num_updates)] for i in range(len(total_times)) ]
    for i in range(len(ks)):
        print("\nk = " + str(ks[i]))
        for j in range(len(time_per_update)//len(ks)):
            time = time_per_update[i*len(ks) + j]*1000.
            print(f"{time:.{2}f}")

num_updates = read_number_of_updates("graph_info.txt")
total_times = read_total_times("8_bestfrac.txt")
print_times_per_update(num_updates, total_times)
